﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moviment : MonoBehaviour
{
    public float speed;
    public float jumpForce;
    //cantitat de força que es aplicada al estar al aire
    public float airMove;
    private bool Grounded = true;
    Rigidbody rigidbody;
    private Actions act;
    private Vector3 scale;
    GameObject obj;

    public enum Status { NONE, ATTACKED, IDLE, JUMP, RUNNING };
    public Status playerStatus;

    private void Awake()
    {
        this.rigidbody = this.gameObject.GetComponent<Rigidbody>();
        act = gameObject.GetComponent<Actions>();
        playerStatus = Status.IDLE;
        scale = this.transform.localScale;

    }
    
    void Start()
    {
        
    }

    
    void Update()
    {
        
        if (this.Grounded && Time.timeScale > 0.5)
        {
            girarPj();
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            this.gameObject.GetComponent<cameraManager>().iniciarCanvi();
        }
        if (this.Grounded)
        {
            if (this.gameObject.GetComponent<cameraManager>().cenitalMode)
            {
                movimentCenital();
            }
            else
            {
                movimentLateral();
            }
        }
        else
        {
            if (this.gameObject.GetComponent<cameraManager>().cenitalMode)
            {
                airMovimentCenital();
            }
            else
            {
                airMovimentLateral();
            }
        }

        checkAnimation();

    }

    private void girarPj()
    {
        if (this.gameObject.GetComponent<cameraManager>().cenitalMode)
        {
            float moveVertical = Input.GetAxis("Vertical");
            float moveHorizontal = Input.GetAxis("Horizontal");

            Vector3 newPosition = new Vector3(moveHorizontal, 0, moveVertical);
            transform.LookAt(newPosition + transform.position);
        } else
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            if (moveHorizontal > 0)
            {
                moveHorizontal = 1;
            } else if (moveHorizontal < 0)
            {
                moveHorizontal = -1;
            }
            

            Vector3 newPosition = new Vector3(0, 0, moveHorizontal);
            transform.LookAt(newPosition + this.transform.position);
        }
        
    }

    void movimentCenital()
    {
        this.rigidbody.velocity = new Vector3(Input.GetAxis("Horizontal") * speed 
            * Time.deltaTime, this.rigidbody.velocity.y, Input.GetAxis("Vertical") * speed * Time.deltaTime);  
    }

    void movimentLateral()
    {
        
        this.rigidbody.velocity = new Vector3(0, this.rigidbody.velocity.y, Input.GetAxis("Horizontal") * speed * Time.deltaTime);
        if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space)) && this.Grounded)
        {
            this.Grounded = false;
            this.saltar();
        }
    }

    void airMovimentCenital()
    {
        this.rigidbody.AddForce(new Vector3(Input.GetAxis("Horizontal")*this.airMove, 0, Input.GetAxis("Vertical")*this.airMove));
    }

    void airMovimentLateral()
    {
        
        this.rigidbody.AddForce(new Vector3(0, 0, Input.GetAxis("Horizontal")*this.airMove));
    }

    void saltar()
    {
        this.rigidbody.AddForce(0, jumpForce, 0);
        if(playerStatus != Status.RUNNING)
        {
            act.Jump();
            playerStatus = Status.JUMP;
        }
            
    }

    public void setGroundedToTrue()
    {
        this.Grounded = true;
        act.Stay();
    }

    public void checkAnimation()
    {
        if (rigidbody.velocity.magnitude > 0 && playerStatus != Status.ATTACKED)
        {
            playerStatus = Status.RUNNING;
            act.Run();
        }
        else
        {
            act.Stay();
            playerStatus = Status.IDLE;
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            act.Stay();
        }

        if (collision.gameObject.CompareTag("ChangePosObject"))
        {
            obj = new GameObject();
            obj.transform.parent = collision.transform;
            transform.parent = obj.transform;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("ChangePosObject"))
        {
            gameObject.transform.parent = null;
            Destroy(obj.gameObject);
        }
    }
}