﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingCol : MonoBehaviour
{
    private Vector3 pos;
    private Quaternion rot;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        this.GetComponent<Rigidbody>().velocity = this.transform.forward * 25f;
    }

    public void setPos(Vector3 pos)
    {
        this.transform.position = pos;
    }

    public void setRot(Quaternion rot)
    {
        this.transform.rotation = rot;
    }

}