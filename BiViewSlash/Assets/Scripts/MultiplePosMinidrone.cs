﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplePosMinidrone : MonoBehaviour
{
    private float heightChange = 3f;

    private void Awake()
    {
    }
    void Start()
    {
        GameEvents.canviDeVista.onCenitalChange += cenitalChange;
        GameEvents.canviDeVista.onLateralChange += lateralChange;
    }

    private void cenitalChange()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - this.heightChange, this.transform.position.z);
    }

    private void lateralChange()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + this.heightChange, this.transform.position.z);
    }

    private void OnDestroy()
    {
        GameEvents.canviDeVista.onCenitalChange -= cenitalChange;
        GameEvents.canviDeVista.onLateralChange -= lateralChange;
    }
}
