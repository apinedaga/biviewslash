﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class multiplePosPlatform : MonoBehaviour
{
    public Vector3 pos1;
    public Vector3 pos2;

    private void Awake()
    {
        this.transform.localPosition = pos1;
    }
    void Start()
    {
        GameEvents.canviDeVista.onCenitalChange += cenitalChange;
        GameEvents.canviDeVista.onLateralChange += lateralChange;
    }

    private void cenitalChange()
    {
        this.transform.localPosition = pos1;
    }

    private void lateralChange()
    {
        this.transform.localPosition = pos2;
    }
}
