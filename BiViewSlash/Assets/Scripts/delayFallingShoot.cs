﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class delayFallingShoot : MonoBehaviour
{
    private bool valueSetted = false;
    private bool targetCached = false;
    private bool timeToGoDown = false;
    [SerializeField] private float speed = 6f;
    [SerializeField]  private float fallingDelay = 1f;
    [SerializeField] private float damage = 10f;
    [SerializeField] private SO_Player player;
    [SerializeField] private HpManagerEvent hpEvent;
    private float lifeTime = 6f;
    private Vector3 target;

    private float heightChange = 10f;

    private GameEvents gameEvents;

    private void Start()
    {
        GameEvents.canviDeVista.onCenitalChange += OnCenitalChange;
        GameEvents.canviDeVista.onLateralChange += OnLateralChange;
    }

    public void setMyTarget(Vector3 pos)
    {
        this.target = new Vector3(pos.x, this.transform.position.y, pos.z);
        this.valueSetted = true;

    }

    private IEnumerator followFallDelay()
    {
        yield return new WaitForSecondsRealtime(this.fallingDelay);
        foreach (Transform child in this.transform)
        {
            // destrueix els aros de energia abans de caure
            Destroy(child.gameObject);
        }
        this.timeToGoDown = true;
    }

    private IEnumerator lifetimeClock()
    {
        yield return new WaitForSecondsRealtime(this.lifeTime);
        Destroy(this.gameObject);
    }

    private void Update()
    {
        if (this.valueSetted && !this.targetCached)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, this.target, this.speed*Time.deltaTime);
            if (this.transform.position.x == target.x && this.transform.position.z == target.z)
            {
                this.targetCached = true;
                this.StartCoroutine(this.followFallDelay());
            }
        }

        if (this.timeToGoDown)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(this.transform.position.x, Vector3.down.y, this.transform.position.z), this.speed * Time.deltaTime);
            this.StartCoroutine(this.lifetimeClock());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player" && !player.invencible)
        {
            this.player.hp -= this.damage;
            this.hpEvent.RaiseItem(this.damage);
            Destroy(this.gameObject);
        } else if (other.transform.tag == "Ground")
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCenitalChange()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - this.heightChange, this.transform.position.z);
        this.target = new Vector3(target.x, this.transform.position.y, target.z);
    }
    
    private void OnLateralChange()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + this.heightChange, this.transform.position.z);
        this.target = new Vector3(target.x, this.transform.position.y, target.z);
    }

    private void OnDestroy()
    {
        GameEvents.canviDeVista.onCenitalChange -= OnCenitalChange;
        GameEvents.canviDeVista.onLateralChange -= OnLateralChange;
    }

}
