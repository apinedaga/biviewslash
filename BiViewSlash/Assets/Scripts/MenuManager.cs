﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public GameObject hpHud;
    public GameObject menu;
    public EventSystem eventSystem;
    private float[] btnScale;
    private GameObject[] btnSelected;
    private int currentButton = 0;
    private int maxButton = 6;
    private bool inputDetected = false;
    private bool up = false;
    public SO_Player pjStatus;
    public CameraType cam;
    public Camera camCenital;
    public Camera camLateral;
    private GameObject status;
    private GameObject controls;
    private GameObject save;
    private GameObject load;
    private GameObject sound;
    public GameObject saveManager;
    [Range(0, 1)]
    public float m_MySliderValue;
    public Slider soundSlider;
    public SO_Sound soundSO;
    public AudioSource m_MyAudioSource;
    public bool menuOpened = false;

    Image hpBarImage;
    Image mpBarImage;

    [Range(0, 1)]
    float hpBarProgress = 0;
    [Range(0, 1)]
    float mpBarProgress = 0;

    private bool upAxisGamepadPressed = false;
    private bool downAxisGamepadPressed = false;
    // Start is called before the first frame update
    void Start()
    {
        //m_MyAudioSource = GetComponent<AudioSource>();
        btnSelected = new GameObject[maxButton+1];
        for (int i = 0; i <= maxButton; i++)
        {
            btnSelected[i] = menu.GetComponent<RectTransform>().GetChild(1).GetChild(0).GetChild(i).gameObject;
        }
        /*
         * Assignes cada menu, afagant els fills des del canvas
         */
        menu.SetActive(false);
        status = menu.GetComponent<RectTransform>().GetChild(1).GetChild(1).gameObject;
        status.SetActive(false);
        controls = menu.GetComponent<RectTransform>().GetChild(1).GetChild(2).gameObject;
        save = menu.GetComponent<RectTransform>().GetChild(1).GetChild(3).gameObject;
        load = menu.GetComponent<RectTransform>().GetChild(1).GetChild(4).gameObject;
        sound = menu.GetComponent<RectTransform>().GetChild(1).GetChild(5).gameObject;
        status.SetActive(false);
        controls.SetActive(false);
        save.SetActive(false);
        hpBarImage = menu.GetComponent<RectTransform>().GetChild(1).GetChild(1).GetChild(2).GetComponent<Image>();
        mpBarImage = menu.GetComponent<RectTransform>().GetChild(1).GetChild(1).GetChild(3).GetComponent<Image>();
        FillBar();
        if (soundSO.loaded)
        {
            soundSlider.value = soundSO.soundVolume;
            m_MySliderValue = soundSO.soundVolume;
            soundSO.loaded = false;
        }
        else
        {
            soundSlider.value = soundSO.soundVolume;
            m_MySliderValue = soundSlider.value;
        }
        
    }

    /*
     * Actualitzes les barres de vida i energia al valor del jugador actualment
     */
    private void FillBar()
    {
        if (pjStatus.hp == pjStatus.hpMax)
            hpBarProgress = 1;
        else if (pjStatus.hp <= 0)
            hpBarProgress = 0;
        else
            hpBarProgress = 1-(pjStatus.hpMax - pjStatus.hp) / pjStatus.hpMax;

        hpBarImage.fillAmount = hpBarProgress;

        if (pjStatus.mp == pjStatus.mpMax)
            hpBarProgress = 1;
        else if (pjStatus.mp <= 0)
            hpBarProgress = 0;
        else
            mpBarProgress = 1-(pjStatus.mpMax - pjStatus.mp) / pjStatus.mpMax;

        mpBarImage.fillAmount = hpBarProgress;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.P) || Input.GetKey(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button7))
        {
            resetMenu();
            OpenMenu();
            pjStatus.invencible = true;
            
        }
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) || ((Input.GetAxis("Vertical") < -0.5f || Input.GetAxis("dpadUpDown") < -0.5f) && !this.downAxisGamepadPressed))
        {
            this.downAxisGamepadPressed = true;
            up = false;
            MoveDown();
        }
        else if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) || ((Input.GetAxis("Vertical") > 0.5f || Input.GetAxis("dpadUpDown") > 0.5f) && !this.upAxisGamepadPressed))
        {
            this.upAxisGamepadPressed = true;
            up = true;
            MoveUp();
        }

        if ((Input.GetAxis("Vertical") > -0.5f && Input.GetAxis("dpadUpDown") > -0.5f) && this.downAxisGamepadPressed)
        {
            this.downAxisGamepadPressed = false;
        }
        if ((Input.GetAxis("Vertical") < 0.5f && Input.GetAxis("dpadUpDown") < 0.5f) && this.upAxisGamepadPressed)
        {
            this.upAxisGamepadPressed = false;
        }

        if ((Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Joystick1Button0)) && menuOpened)
        {
            btnSelected[currentButton].GetComponent<Button>().onClick.Invoke();
        }

        /*if (inputDetected)
            BtnChange();*/
       
    }

    private void resetMenu()
    {
        foreach (GameObject item in btnSelected)
        {
            item.GetComponent<Outline>().enabled = false;
            item.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1);
        }
        
    }

    /*
     * Selecció del botó
     */
    private void BtnChange()
    {

        inputDetected = false;
        int aux = 0;
        if (currentButton == 0 && !up)
            aux = 0;
        else if(up)
            aux = currentButton+1;
        else
            aux = currentButton - 1;

        btnSelected[aux].GetComponent<Outline>().enabled = false;
        //btnSelected[currentButton].GetComponent<Outline>().enabled = true;

        //btnSelected[currentButton].GetComponent<RectTransform>().localScale = new Vector3(1.5f, 1.5f, 1);
        //btnSelected[aux].GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
    }
    /*
     * Tornes al joc
     */
    public void BtnContinue()
    {
        menuOpened = false;
        resetMenuOptions();
        eventSystem.SetSelectedGameObject(null);
        currentButton = 0;
        menu.SetActive(false);
        hpHud.SetActive(true);
        pjStatus.invencible = false;
        Time.timeScale = 1;

    }
    /*
     * Mostra el menu amb les estadistiques del jugador
     */
    public void BtnStatus()
    {
        resetMenuOptions();
        FillBar();
        status.GetComponent<RectTransform>().GetChild(2).GetComponentInChildren<TextMeshProUGUI>().text = "HP: "+pjStatus.hp+" / "+pjStatus.hpMax ;
        status.GetComponent<RectTransform>().GetChild(3).GetComponentInChildren<TextMeshProUGUI>().text = "MP: " + pjStatus.mp + " / " + pjStatus.mpMax;
        status.GetComponent<RectTransform>().GetChild(4).GetComponent<TextMeshProUGUI>().text = "ATTACK: " + pjStatus.attack;
        status.GetComponent<RectTransform>().GetChild(5).GetComponent<TextMeshProUGUI>().text = "DEFENSE: " + pjStatus.defense;
        status.SetActive(true);
    }

    /*
     * Menu amb el volum del so
     */
    public void BtnSound()
    {
        resetMenuOptions();
        sound.SetActive(true);
    }

    /*
     * Resetejes tots els menus al obrir o canviar d'opció
     */
    private void resetMenuOptions()
    {
        status.SetActive(false);
        controls.SetActive(false);
        save.SetActive(false);
        load.SetActive(false);
        sound.SetActive(false);

    }

    private void OpenMenu()
    {
        menuOpened = true;
        resetMenuOptions();
        currentButton = 0;
        hpHud.SetActive(false);
        if (cam.cenital)
             menu.GetComponent<Canvas>().worldCamera = camCenital;
        else
            menu.GetComponent<Canvas>().worldCamera = camLateral;

        menu.SetActive(true);
        resetMenu();
        Time.timeScale = 0;
        eventSystem.SetSelectedGameObject(btnSelected[0]);
        eventSystem.firstSelectedGameObject= btnSelected[0];
        //btnSelected[0].GetComponent<Outline>().enabled = true;
        //btnSelected[0].GetComponent<RectTransform>().localScale = new Vector3(1.5f, 1.5f, 1);
    }

    private void MoveDown()
    {
        currentButton++;
        if (currentButton > maxButton)
            currentButton = maxButton;
        BtnChange();
        inputDetected = true;
        
    }

    private void MoveUp()
    {
        
        currentButton--;
        if (currentButton < 0)
            currentButton = 0;
        BtnChange();
        inputDetected = true;
        
    }

    /*
     * Obre el menu amb els controls
     */
    public void BtnControls()
    {
        resetMenuOptions();
        controls.SetActive(true);
    }
    /*
     * Menu per guardar
     */
    public void BtnSave()
    {
        resetMenuOptions();
        foreach (GameObject item in btnSelected)
        {
            item.GetComponent<Button>().interactable = false;
        }
        save.SetActive(true);
        eventSystem.SetSelectedGameObject(save.transform.GetChild(2).gameObject);
    }

    public void BtnSaveYes()
    {
        resetMenuOptions();
        foreach (GameObject item in btnSelected)
        {
            item.GetComponent<Button>().interactable = true;
        }
        currentButton = 0;
        saveManager.GetComponent<SaveManager>().SaveGame();
        menu.SetActive(false);
        hpHud.SetActive(true);
        pjStatus.invencible = false;
        Time.timeScale = 1;
    }

    public void BtnSaveNo()
    {
        resetMenuOptions();
        foreach (GameObject item in btnSelected)
        {
            item.GetComponent<Button>().interactable = true;
        }
        currentButton = 0;
        menu.SetActive(false);
        hpHud.SetActive(true);
        pjStatus.invencible = false;
        Time.timeScale = 1;

    }

    /*
     * Menu per carregar
     */
    public void BtnLoad()
    {
        resetMenuOptions();
        foreach (GameObject item in btnSelected)
        {
            item.GetComponent<Button>().interactable = false;
        }
        load.SetActive(true);
        eventSystem.SetSelectedGameObject(load.transform.GetChild(2).gameObject);
    }

    public void BtnLoadYes()
    {
        resetMenuOptions();
        foreach (GameObject item in btnSelected)
        {
            item.GetComponent<Button>().interactable = true;
        }
        currentButton = 0;
        saveManager.GetComponent<SaveManager>().LoadGame();
        menu.SetActive(false);
        hpHud.SetActive(true);
        pjStatus.invencible = false;
        Time.timeScale = 1;
    }

    public void BtnLoadNo()
    {
        resetMenuOptions();
        foreach (GameObject item in btnSelected)
        {
            item.GetComponent<Button>().interactable = true;
        }
        currentButton = 0;
        menu.SetActive(false);
        hpHud.SetActive(true);
        pjStatus.invencible = false;
        Time.timeScale = 1;

    }

    void OnGUI()
    {
        
        m_MySliderValue = soundSlider.value;
        
        m_MyAudioSource.volume = m_MySliderValue;
    }

    public void BtnMainMenu()
    {
        SceneManager.LoadScene("Start");
    }

    public void updateSound()
    {
        soundSO.soundVolume = soundSlider.value;
    }
}


