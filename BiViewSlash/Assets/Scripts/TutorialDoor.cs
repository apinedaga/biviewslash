﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDoor : MonoBehaviour
{
    [SerializeField] private TutorialPuzzleStatus myStatus;
    [SerializeField] private GameObject[] mySpheres;

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "key")
        {
            this.myStatus.solved2 = true;
            other.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        if (this.myStatus.solved1)
        {
            this.mySpheres[0].GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else
        {
            this.mySpheres[0].GetComponent<MeshRenderer>().material.color = Color.yellow;
        }

        if (this.myStatus.solved2)
        {
            this.mySpheres[1].GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else
        {
            this.mySpheres[1].GetComponent<MeshRenderer>().material.color = Color.yellow;
        }

        if (this.myStatus.solved3)
        {
            this.mySpheres[2].GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else
        {
            this.mySpheres[2].GetComponent<MeshRenderer>().material.color = Color.yellow;
        }

        if (this.myStatus.solved1 && this.myStatus.solved2 && this.myStatus.solved3)
        {
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
        } else
        {
            this.gameObject.GetComponent<BoxCollider>().enabled = true;
            this.gameObject.GetComponent<MeshRenderer>().enabled = true;
        }
    }
}
