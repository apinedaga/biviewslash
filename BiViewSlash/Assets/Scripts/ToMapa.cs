﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToMapa : MonoBehaviour
{
    public TutorialPuzzleStatus tutP;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (tutP.solved1 && tutP.solved2 && tutP.solved3)
            {
                SceneManager.LoadScene("Mapa");
            }
        }
    }
 
}
