﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager : MonoBehaviour
{
    public List<GameObject> objectsToChange = new List<GameObject>();
    public List<GameObject> objectToDesactivate = new List<GameObject>();
    public List<GameObject> enemiesToDesactivate = new List<GameObject>();
    public List<GameObject> respawnToDesactivate = new List<GameObject>();
    public CameraType camType;
    public SO_Player player;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
            
    }

    /*
     * Recorres la llista i invoques la funció RaiseItem del Listener
     */
    public void changePositionObjects()
    {
        foreach (GameObject item in objectsToChange)
        {
            item.GetComponent<ObjectPosition>().objectEvent.RaiseItem(item);

        }

    }

    /*
     * Recorres la llista i invoques la funció RaiseItem del Listener
     */
    public void ChangeObjectActivation()
    {
        if(objectToDesactivate.Count != 0)
        {
            foreach (GameObject item in objectToDesactivate)
            {
                if (item.transform.position.x > player.posicio.x)
                {
                    //print("HOLA");
                    item.GetComponent<ObjectActivation>().objectEvent.RaiseItem(item);
                }
            }
        }
    }

    /*
     * Recorres la llista i invoques la funció RaiseItem del Listener
     */
    public void EnemyActivation()
    {
        foreach (GameObject item in enemiesToDesactivate)
        {
            if (item.transform.position.x > player.posicio.x)
            {
                item.GetComponent<EnemyActivation>().enemyEvent.RaiseItem(item);
            }
               

        }
    }

    /*
     * Recorres la llista i invoques la funció RaiseItem del Listener
     */
    public void RespawnActivation()
    {
        foreach (GameObject item in respawnToDesactivate)
        {
            if (item.transform.position.x > player.posicio.x)
            {
                item.GetComponent<RespawnActivation>().objectEvent.RaiseItem(item);
            }


        }
    }


}
