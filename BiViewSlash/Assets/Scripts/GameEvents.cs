﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameEvents : MonoBehaviour
{
    public static GameEvents canviDeVista;
    private void Awake()
    {
        canviDeVista = this;
    }


    public event Action onLateralChange;
    public void LateralChangeEvent()
    {
        if (onLateralChange != null)
        {
            onLateralChange();
        }
    }

    public event Action onCenitalChange;
    public void CenitalChangeEvent()
    {
        if (onCenitalChange != null)
        {
            onCenitalChange();
        }
    }
}
