﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveManager : MonoBehaviour
{
    public GameObject player;
    public SO_SaveGame saves;
    public SO_DeadEnemies deadEnemies;
    public GameObject enemies;
    public SO_PuzlesStatus puzleStatus;
    public SO_RespawnsStatus respawnStatus;
    public SO_LastRespawn lastRespawn;
    public SO_Sound sound;
    private string savePath;


    private void Awake()
    {
        //savePath = System.IO.Directory.GetCurrentDirectory() + "/Saves/Save.json";
        savePath = Application.streamingAssetsPath + "\\" + "Save.json";
    }

    public void SaveGame()
    {
        saves.playerPosition = player.transform.position;
        saves.hp = player.GetComponent<setPlayerSO>().stats.hp;
        saves.mp = player.GetComponent<setPlayerSO>().stats.mp;
        saves.lastRespawnPosition = player.GetComponent<Moviment>().respawn.lastRespawn;
        saves.deadEnemies = new string[deadEnemies.deadEnemies.Count];

        for (int i = 0; i < deadEnemies.deadEnemies.Count; i++)
        {
            saves.deadEnemies[i] = deadEnemies.deadEnemies[i];
        }

        saves.respawnFirstDoor = respawnStatus.respawnFirstDoor;
        saves.respawnBossDoor = respawnStatus.respawnBossDoor;
        saves.respawnBossFloor = respawnStatus.respawnBossFloor;

        saves.puzleDoor = puzleStatus.puzleDoor;
        saves.puzleBossRight = puzleStatus.puzleBossRight;
        saves.puzleBossLeft = puzleStatus.puzleBossLeft;
        saves.currentScene = SceneManager.GetActiveScene().name;

        saves.soundValue = sound.soundVolume;

        string jsonStr = JsonUtility.ToJson(saves, true);
        //File.WriteAllText("Assets/Saves/Save.json", jsonStr);
        File.WriteAllText(savePath, jsonStr);
    }

    public void LoadGame()
    {
        //string jsonStr = File.ReadAllText("Assets/Saves/Save.json");
        string jsonStr = File.ReadAllText(savePath);
        JsonUtility.FromJsonOverwrite(jsonStr, saves);

        player.GetComponent<setPlayerSO>().stats.savedPosition = saves.playerPosition;
        player.GetComponent<setPlayerSO>().stats.hp = saves.hp;
        player.GetComponent<setPlayerSO>().stats.mp = saves.mp;

        player.GetComponent<Moviment>().respawn.lastRespawn = saves.lastRespawnPosition;

        deadEnemies.deadEnemies.Clear();

        for (int i = 0; i < saves.deadEnemies.Length; i++)
        {
            deadEnemies.deadEnemies.Add(saves.deadEnemies[i]);
        }
        

        puzleStatus.puzleDoor = saves.puzleDoor;
        puzleStatus.puzleBossRight = saves.puzleBossRight;
        puzleStatus.puzleBossLeft = saves.puzleBossLeft;

        respawnStatus.respawnFirstDoor = saves.respawnFirstDoor;
        respawnStatus.respawnBossDoor = saves.respawnBossDoor;
        respawnStatus.respawnBossFloor = saves.respawnBossFloor;


        deadEnemies.loaded = true;
        player.GetComponent<setPlayerSO>().stats.loaded = true;
        player.GetComponent<setPlayerSO>().stats.loadPos = true;
        sound.soundVolume = saves.soundValue;
        sound.loaded = true;
        SceneManager.LoadScene(saves.currentScene);
    }
}
