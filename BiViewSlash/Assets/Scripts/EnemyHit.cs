﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHit : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerHurtBox"))
        {
            this.GetComponentInParent<EnemyAttack>().setAttacking(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PlayerHurtBox"))
        {
            this.GetComponentInParent<EnemyAttack>().setAttacking(false);
        }
    }
}
