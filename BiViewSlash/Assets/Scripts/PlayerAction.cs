﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Classe que controla les animacions del pj.
 */
[RequireComponent(typeof(Animator))]
public class PlayerAction : MonoBehaviour
{
    private Animator animator;

	const int countOfDamageAnimations = 3;
	int lastDamageAnimation = -1;

	void Awake()
    {
        animator = GetComponent<Animator>();
    }

	public void Stay()
	{
		animator.SetFloat("Speed", 0f);
	}

	public void Walk()
	{
		animator.SetFloat("Speed", 0.1f);
	}

	public void Run()
	{
		animator.SetFloat("Speed", 0.5f);
	}

	public void Attack()
	{
		animator.SetBool("Attack", true);
	}
	public void Combo1()
	{
		animator.SetBool("Combo1", true);
	}

	public void AttackOff()
    {
		animator.SetBool("Attack", false);
	}

	public void Combo1Off()
	{
		animator.SetBool("Combo1", false);
	}

	public void Ulti()
    {
		animator.SetBool("Ulti", true);
	}

	public void UltiOff()
	{
		animator.SetBool("Ulti", false);
	}

	public void Death()
    {
		animator.SetFloat("Speed", 0f);
		animator.SetBool("Combo1", false);
		animator.SetBool("Ulti", false);
		animator.SetBool("Death", true);
	}

	public void DeathOff()
	{
		animator.SetBool("Death", false);
	}

    internal void hitted()
    {
		animator.SetBool("Hitted", true);
	}

    internal void hittedOff()
    {
		animator.SetBool("Hitted", false);
	}
}
