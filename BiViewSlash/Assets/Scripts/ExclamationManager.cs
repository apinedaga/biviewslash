﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExclamationManager : MonoBehaviour
{
    private float raycastLenght = 3.5f;
    private float exclamationElevation = 3f;
    public GameObject exclamacioCenital;
    public GameObject exclamacioLateral;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = new Vector3(this.GetComponentInParent<Transform>().position.x, this.GetComponentInParent<Transform>().position.y + this.exclamationElevation, this.GetComponentInParent<Transform>().position.z);
        this.exclamacioCenital.transform.position = pos;
        this.exclamacioLateral.transform.position = pos;
        RaycastHit groundCheckHit;
        Ray groundCheckRay = new Ray(this.transform.position, Vector3.down);
        if (!Physics.Raycast(groundCheckRay, out groundCheckHit, this.raycastLenght))
        {
            if (this.GetComponentInParent<cameraManager>().cenitalMode)
            {
                this.exclamacioCenital.SetActive(true);
                this.exclamacioLateral.SetActive(false);
            }
            else
            {
                this.exclamacioCenital.SetActive(false);
                this.exclamacioLateral.SetActive(true);
            }
        }
        else
        {
            this.exclamacioCenital.SetActive(false);
            this.exclamacioLateral.SetActive(false);
        }
    }
}
