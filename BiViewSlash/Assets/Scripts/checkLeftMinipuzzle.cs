﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkLeftMinipuzzle : MonoBehaviour
{
    [SerializeField] private MiniPuzzleStatus myStatus;
    [SerializeField] private GameObject[] mySpheres;
    [SerializeField] private GameObject[] myCubes;
    [SerializeField] private GameObject[] myActivators;
    public SO_PuzlesStatus pStatus;

    private void Start()
    {
        if (!pStatus.puzleBossLeft)
        {
            this.myStatus.solved1 = false;
            this.myStatus.solved2 = false;
        }
        if (this.myStatus.solved1)
        {
            Vector3 target = new Vector3(this.myActivators[0].transform.position.x, this.myActivators[0].transform.position.y + 1, this.myActivators[0].transform.position.z);
            this.myCubes[0].transform.position = target;
        }

        if (this.myStatus.solved2)
        {
            Vector3 target = new Vector3(this.myActivators[1].transform.position.x, this.myActivators[1].transform.position.y + 1, this.myActivators[1].transform.position.z);
            this.myCubes[1].transform.position = target;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (this.myStatus.solved1 && this.myStatus.solved2)
        {
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
            pStatus.puzleBossLeft = true;
        } else
        {
            this.gameObject.GetComponent<MeshRenderer>().enabled = true;
            this.gameObject.GetComponent<BoxCollider>().enabled = true;
        }

        if (this.myStatus.solved1)
        {
            this.mySpheres[0].GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else
        {
            this.mySpheres[0].GetComponent<MeshRenderer>().material.color = Color.red;
        }

        if (this.myStatus.solved2)
        {
            this.mySpheres[1].GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else
        {
            this.mySpheres[1].GetComponent<MeshRenderer>().material.color = Color.red;
        }

    }
}
