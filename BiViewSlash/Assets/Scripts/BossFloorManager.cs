﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFloorManager : MonoBehaviour
{
    public SO_Player player;
    public GameObject bridge;
    public GameObject PlayerPrefab;
    public GameObject respawn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(player.hp <= 0 || PlayerPrefab.transform.position.y <= -4)
        {
            bridge.SetActive(true);
            respawn.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
            respawn.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
            respawn.transform.GetChild(2).GetComponent<MeshRenderer>().enabled = true;
        }
    }
}
