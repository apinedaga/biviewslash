﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossManager : MonoBehaviour
{
    private int hittedTimesToChangeSpot = 2;
    private int hittedTimes = 0;

    private int fases = 3;
    [SerializeField] private int actualFase = 1;
    //public SO_EnemyStats stats;
    private float hp = 500;
    private float resetHp = 500;
    public SO_Player playerStatus;
    public HpManagerEvent hpEvent;
    private float heightChange = 10f;
    private enum EnemyStatus { READY, ATTACKING, WAITING};
    private EnemyStatus attackStatus;
    public GameObject bomZone;
    private bool zoneCreated = false;
    private GameObject newZone;
    
    private BossAttackManager attackManager;

    [SerializeField] private GameObject hurtBox;
    [SerializeField] private GameObject[] lateralSpots;
    [SerializeField] private GameObject[] cenitalSpots;

    private bool playerDetected = false;
    public GameObject bridgeEnd;
    public GameObject[] deathSpots1;
    public GameObject[] deathSpots2;
    public GameObject deathEffect;
    private float size;
    public GameObject canvasHp;

    public GameObject mainCam;
    public Camera cam;

    [SerializeField] private Material lateralMat;
    [SerializeField] private Material cenitalMat;

    private void Awake()
    {
        this.attackManager = this.gameObject.GetComponent<BossAttackManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        size = deathSpots1.Length;
        GameEvents.canviDeVista.onCenitalChange += cenitalChange;
        GameEvents.canviDeVista.onLateralChange += lateralChange;
        this.hurtBox.transform.position = this.lateralSpots[0].transform.position;
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - this.heightChange, this.transform.position.z);
        attackStatus = EnemyStatus.READY;
       
    }

    // Update is called once per frame
    void Update()
    {
        if (this.playerDetected)
        {
            switch (this.actualFase)
            {
                case 1:
                    this.attackManager.attackFase1();
                    break;
                case 2:
                    this.attackManager.attackFase2();
                    break;
                case 3:
                    this.attackManager.attackFase3();
                    break;
            }
        }
        /*if(playerStatus.hp <= 0)
        {
            this.actualFase = 1;
            this.hp = resetHp;
            size = deathSpots1.Length;
            this.hurtBox.transform.position = this.lateralSpots[0].transform.position;
            attackStatus = EnemyStatus.READY;
        }*/
        
    }

    public void setPlayerInRange(bool b)
    {
        this.playerDetected = b;
    }

    private void Attacking()
    {
        attackStatus = EnemyStatus.WAITING;
        newZone = Instantiate(bomZone);
        newZone.transform.position = playerStatus.posicio;
        StartCoroutine(BomAttack());
    }

    IEnumerator BomAttack()
    {
        yield return new WaitForSecondsRealtime(2f);
        
        Destroy(newZone);
        yield return new WaitForSecondsRealtime(3f);
        
        attackStatus = EnemyStatus.READY;
    }

    IEnumerator ChangeToAttack()
    {
        yield return new WaitForSecondsRealtime(3f);
        if(attackStatus == EnemyStatus.READY)
        {
            attackStatus = EnemyStatus.ATTACKING;
            zoneCreated = false;
        }
            
    }

    private Vector3 getRandomSpot()
    {
        Vector3 trans = new Vector3();
        System.Random r = new System.Random();
        do
        {
            switch (this.actualFase)
            {
                case 1:
                    trans = this.lateralSpots[r.Next(0, this.lateralSpots.Length)].transform.position;
                    break;

                case 2:
                    trans = this.cenitalSpots[r.Next(0, this.cenitalSpots.Length)].transform.position;
                    break;

                default:
                    int nextIsCenital = r.Next(2);
                    if (nextIsCenital >= 1)
                    {
                        trans = this.lateralSpots[r.Next(0, this.lateralSpots.Length)].transform.position;
                        this.hurtBox.GetComponent<MeshRenderer>().material = lateralMat;
                    }
                    else
                    {
                        trans = this.cenitalSpots[r.Next(0, this.cenitalSpots.Length)].transform.position;
                        this.hurtBox.GetComponent<MeshRenderer>().material = cenitalMat;
                    }
                    break;
            }
        } while(this.hurtBox.transform.position == trans);
        return trans;
    }

    public void ReciveDamage(float dmg)
    {
        this.hp -= dmg;
        this.hittedTimes++;
        if (this.hittedTimes >= this.hittedTimesToChangeSpot)
        {
            this.hittedTimes = 0;
            this.hurtBox.transform.position = this.getRandomSpot();
        }
        if (this.hp <= 0)
        {
            if (this.actualFase < this.fases)
            {
                this.ChangeFase();
            } else
            {
                /*
                 * Si el boss mort, s'activa el efecte de la seva mort.
                 */
                this.hurtBox.gameObject.GetComponent<MeshRenderer>().enabled = false; //es fa la hurtbox invisible per a que no surti a la cinematica
                canvasHp.SetActive(false); //desactives el canvas perque noe s vegi
                this.transform.GetChild(4).gameObject.SetActive(false); // go per detectar al pj, aixi no pot atacar mentres mort
                setPlayerInRange(false); //Desactives el persoantge detectat
                playerStatus.invencible = true; //No perd vida
                mainCam.SetActive(false); // desactives les cameres principals
                cam.gameObject.SetActive(true); //Actives la camera que apunta al boss
                bridgeEnd.SetActive(true);
                StartCoroutine(BossEnd1()); //Corrutina amb explosions 1
                StartCoroutine(BossEnd2()); //Corrutina amb explosions 2
                StartCoroutine(BossEnd()); //corrutina on es destrueix al boss.
                //Destroy(this.gameObject);
            }
        }
    }
    /*
     * Activa una corrutina on s'actives efectes de particles d'explosions al morir el boss
     * Es combina amb una altre corrutina BossEnd2
     */
    IEnumerator BossEnd1()
    {
        for (int i = 0; i < size; i++)
        {
            GameObject go = Instantiate(deathEffect);
            go.transform.position = deathSpots1[i].transform.position;
        }
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < size; i++)
        {
            GameObject go = Instantiate(deathEffect);
            go.transform.position = deathSpots1[i].transform.position;
        }
        yield return new WaitForSecondsRealtime(0.4f);
        for (int i = 0; i < size; i++)
        {
            GameObject go = Instantiate(deathEffect);
            go.transform.position = deathSpots1[i].transform.position;
        }
        yield return new WaitForSecondsRealtime(0.5f);
    }

    /*
     * Activa una corrutina on s'actives efectes de particles d'explosions al morir el boss
     * Es combina amb una altre corrutina BossEnd1
     */
    IEnumerator BossEnd2()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        for (int i = 0; i < size; i++)
        {
            GameObject go = Instantiate(deathEffect);
            go.transform.position = deathSpots1[i].transform.position;
        }
        yield return new WaitForSecondsRealtime(0.4f);
        for (int i = 0; i < size; i++)
        {
            GameObject go = Instantiate(deathEffect);
            go.transform.position = deathSpots1[i].transform.position;
        }
        yield return new WaitForSecondsRealtime(0.4f);
        for (int i = 0; i < size; i++)
        {
            GameObject go = Instantiate(deathEffect);
            go.transform.position = deathSpots1[i].transform.position;
        }
    }

    /*
     * Espera a que les corrutines BossEnd1 i BossEnd2 acabin i destrueix el boss.
     */ 
    IEnumerator BossEnd()
    {
        yield return new WaitForSecondsRealtime(3.5f);
        cam.gameObject.SetActive(false);
        mainCam.SetActive(true);
        canvasHp.SetActive(true);
        playerStatus.invencible = false;
        Destroy(this.gameObject);
    }

    private void ChangeFase()
    {
        
        this.actualFase++;
        this.hp = this.resetHp;
        if (actualFase == 2)
        {
            this.hurtBox.transform.position = this.cenitalSpots[0].transform.position;
            this.hurtBox.GetComponent<MeshRenderer>().material = cenitalMat;
        }
        else
        {
            this.hurtBox.transform.position = this.lateralSpots[1].transform.position;
            this.hurtBox.GetComponent<MeshRenderer>().material = lateralMat;
        }
            
    }

    private void cenitalChange()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - this.heightChange, this.transform.position.z);
    }

    private void lateralChange()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + this.heightChange, this.transform.position.z);
    }

    private void OnDestroy()
    {
        GameEvents.canviDeVista.onCenitalChange -= cenitalChange;
        GameEvents.canviDeVista.onLateralChange -= lateralChange;
    }

    public void startBoss()
    {
        this.transform.position = new Vector3(this.transform.position.x, -3.160875f, this.transform.position.z);

    }
}
