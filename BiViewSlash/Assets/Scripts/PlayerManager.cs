﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    //public HpManagerEvent hpEvent;
    public SO_Player stats;

    public Image mpBarImage;

    [Range(0, 1)]
    public float mpBarProgress = 0;


    void Awake()
    {
        //stats.hp = stats.hpMax;
        stats.invencible = false;
    }
    void Start()
    {
        if (stats.mp != stats.mpMax)
            mpBarProgress = 1 - (stats.mpMax - stats.mp) / stats.mpMax;
        else
            mpBarProgress = 1;


        mpBarImage.fillAmount = mpBarProgress;

    }
    void Update()
    {
        if (stats.mp <= 0)
            mpBarProgress = 0;
        else if (stats.mp == stats.mpMax)
            mpBarProgress = 1;
        else
            mpBarProgress = 1 - (stats.mpMax - stats.mp) / stats.mpMax;

        mpBarImage.fillAmount = mpBarProgress;
    }

}
