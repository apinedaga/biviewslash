﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCameraPuzle : MonoBehaviour
{
    public Camera camTrigger;
    public GameObject mainCam;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*
     * Al entrar en contacte amb el pj, desactiva la main Camera i activa la camera fixa en la porta
     * Comença corrutina per desactivar la camera.
     */
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Time.timeScale = 0;
            mainCam.SetActive(false);
            camTrigger.gameObject.SetActive(true);
            StartCoroutine(PuzleCam());
        }
    }

    IEnumerator PuzleCam()
    {
        yield return new WaitForSecondsRealtime(1f);
        camTrigger.gameObject.SetActive(false);
        mainCam.SetActive(true);
        Time.timeScale = 1;
        this.gameObject.SetActive(false);
    }
}
