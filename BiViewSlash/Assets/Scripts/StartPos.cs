﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPos : MonoBehaviour
{
    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        if (!player.GetComponent<setPlayerSO>().stats.loadPos)
        {
            player.transform.position = this.transform.position;
            player.GetComponent<Moviment>().respawn.lastRespawn = this.transform.position;
        }
        else
        {
            player.GetComponent<setPlayerSO>().stats.loadPos = false;
        }
            
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
