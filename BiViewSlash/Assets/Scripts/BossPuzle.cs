﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPuzle : MonoBehaviour
{
    [SerializeField] private GameObject bossDoor;
    public BossPuzleStatus puzzleStatus;
    private bool activated = false;

    /*
     * Canvia el color del material si el bool al SO esta activat
     */
    private void Awake()
    {
        if (this.transform.tag == "PosRight")
        {
            if (this.puzzleStatus.rightSprereActive)
            {
                this.GetComponent<MeshRenderer>().material.color = Color.green;
            } else if (!this.puzzleStatus.rightSprereActive)
            {
                this.GetComponent<MeshRenderer>().material.color = Color.red;
            }
        } else
        {
            if (this.puzzleStatus.leftSphereActive)
            {
                this.GetComponent<MeshRenderer>().material.color = Color.green;
            }
            else if (!this.puzzleStatus.leftSphereActive)
            {
                this.GetComponent<MeshRenderer>().material.color = Color.red;
            }
        }
        if (this.puzzleStatus.leftSphereActive && this.puzzleStatus.rightSprereActive)
        {
            this.bossDoor.GetComponent<BossPuzleDoor>().checkPuzleStatus();
        }
    }

    /*
     * Al colisionar amb la hitbox del pj, canvia el color del material al estat contrari.
     * Si esta activat a verd, sino a vermell. El estat es guarda en un Scriptable Object
     */
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerHitBox"))
        {
            if (!this.activated)
            {
                if (this.transform.tag == "PosRight")
                {
                    this.puzzleStatus.rightSprereActive = true;
                } else
                {
                    this.puzzleStatus.leftSphereActive = true;
                }
                this.GetComponent<MeshRenderer>().material.color = Color.green;
                this.activated = true;
                this.bossDoor.GetComponent<BossPuzleDoor>().SetLeftSphere();
                this.bossDoor.GetComponent<BossPuzleDoor>().SetRightSphere();
            }
            else
            {
                if (this.transform.tag == "PosRight")
                {
                    this.puzzleStatus.rightSprereActive = false;
                }
                else
                {
                    this.puzzleStatus.leftSphereActive = false;
                }

                this.GetComponent<MeshRenderer>().material.color = Color.red;
                this.activated = false;
                this.bossDoor.GetComponent<BossPuzleDoor>().SetLeftSphere();
                this.bossDoor.GetComponent<BossPuzleDoor>().SetRightSphere();
            }
        }
    }
}
