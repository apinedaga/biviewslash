﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public CameraType cam;
    private bool moving = true;
    public enum movingDirection { RIGHT, LEFT };
    public movingDirection currentDir = movingDirection.RIGHT;

    public enum dimension { ZMOVE, XMOVE};
    public dimension currentDimension;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (moving)
        {
            //Es mou la plataforma segons la direcció de moviment asignada, canvia quan colisiona amb l'objecte al extrem.
            if(currentDimension == dimension.ZMOVE)
            {
                if (currentDir == movingDirection.RIGHT)
                {
                    this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - 5 * Time.deltaTime);

                }

                if (currentDir == movingDirection.LEFT)
                {
                    this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + 5 * Time.deltaTime);

                }
            }
            else
            {
                if (currentDir == movingDirection.RIGHT)
                {
                    this.transform.position = new Vector3(this.transform.position.x - 5 * Time.deltaTime, this.transform.position.y, this.transform.position.z);

                }

                if (currentDir == movingDirection.LEFT)
                {
                    this.transform.position = new Vector3(this.transform.position.x + 5 * Time.deltaTime, this.transform.position.y, this.transform.position.z);

                }
            }
        }

    }

    IEnumerator MovingRight()
    {
        moving = false;
        yield return new WaitForSecondsRealtime(1.5f);
        moving = true;
        currentDir = movingDirection.LEFT;

    }

    IEnumerator MovingLeft()
    {

        moving = false;
        yield return new WaitForSecondsRealtime(1.5f);
        moving = true;
        currentDir = movingDirection.RIGHT;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PosRight"))
        {
            StartCoroutine(MovingRight());
        }

        if (other.CompareTag("PosLeft"))
        {
            StartCoroutine(MovingLeft());
        }
    }
}
