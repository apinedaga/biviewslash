﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shootingDroneScript : MonoBehaviour
{
    public GameObject rightCannon;
    public GameObject leftCannon;
    public GameObject basicBullet;
    public SO_Player pj;
    [SerializeField] private float rayDistance = 10f;

    private float cooldownTime = 5f;
    private float betweenShootTime = 0.05f;
    private int clipSize = 5;
    private bool shooting = false;

    private void Update()
    {
        Vector3 lookPoint = new Vector3(pj.posicio.x, this.transform.position.y, pj.posicio.z);
        this.transform.LookAt(lookPoint);

        Vector3 forward = this.transform.TransformDirection(Vector3.forward) * this.rayDistance;
        Vector3 raySpawn = new Vector3(this.transform.position.x, this.rightCannon.transform.position.y, this.transform.position.z);
        Debug.DrawRay(raySpawn, forward, Color.green);

        RaycastHit shootCheckHit;
        Ray shootCheckRay = new Ray(raySpawn, forward * this.rayDistance);
        if (Physics.Raycast(shootCheckRay, out shootCheckHit, this.rayDistance) && !this.shooting)
        {
            StartCoroutine(this.Shoot());
        }
    }

    private IEnumerator Shoot()
    {
        this.shooting = true;
        Vector3 target;

        for (int i = 0; i < this.clipSize; i++)
        {

            target = new Vector3(this.pj.posicio.x, this.rightCannon.transform.position.y, this.pj.posicio.z);
            Instantiate(this.basicBullet, this.rightCannon.transform.position, Quaternion.identity).transform.LookAt(target);
            yield return new WaitForSecondsRealtime(this.betweenShootTime);

            target = new Vector3(this.pj.posicio.x, this.leftCannon.transform.position.y, this.pj.posicio.z);
            Instantiate(this.basicBullet, this.leftCannon.transform.position, Quaternion.identity).transform.LookAt(target);
            yield return new WaitForSecondsRealtime(this.betweenShootTime);
        }

        yield return new WaitForSecondsRealtime(this.cooldownTime);
        this.shooting = false;
    }
}
