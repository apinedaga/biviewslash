﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class unlockedViewsCanvas : MonoBehaviour
{
    private float activationTime = 3f;
    private float timescaleEffect = 0f;
    public void appearingCanvas()
    {
        this.gameObject.SetActive(true);
        StartCoroutine(this.delayDeactivation());
        cameraManager.activeGrayscaleSlowmo(this.activationTime, this.timescaleEffect);
    }

    private IEnumerator delayDeactivation()
    {
        yield return new WaitForSecondsRealtime(this.activationTime);
        this.gameObject.SetActive(false);
    }
}
