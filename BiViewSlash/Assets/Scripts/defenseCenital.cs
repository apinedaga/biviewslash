﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class defenseCenital : MonoBehaviour
{
    private float heightChange = 2.5f;
    private float repelForce = 8000f;
    private ContactPoint contact = new ContactPoint();
    private void Awake()
    {
    }
    void Start()
    {
        GameEvents.canviDeVista.onCenitalChange += cenitalChange;
        GameEvents.canviDeVista.onLateralChange += lateralChange;
    }

    private void cenitalChange()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - this.heightChange, this.transform.position.z);
    }

    private void lateralChange()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + this.heightChange, this.transform.position.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        Collision collision = other.gameObject.GetComponent<Collision>();
        
        Debug.Log("Points colliding: " + collision.contacts.Length);

        
        Debug.Log("Normal of the first point: " + collision.contacts[0].normal);

        
        foreach (var item in collision.contacts)
        {
            Debug.DrawRay(item.point, -item.normal * 100, Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f), 10f);
            collision.gameObject.GetComponent<Rigidbody>().AddForce(-item.normal * this.repelForce);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        Debug.Log("Points colliding: " + collision.contacts.Length);

        
        Debug.Log("Normal of the first point: " + collision.contacts[0].normal);

        
        foreach (var item in collision.contacts)
        {
            Debug.DrawRay(item.point, -item.normal * 100, Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f), 10f);
            collision.gameObject.GetComponent<Rigidbody>().AddForce(-item.normal * this.repelForce);
        }
        
    }


    private void OnTriggerStay(Collider other)
    { 
        if (other.transform.tag == "Player")
        {
            //other.gameObject.GetComponent<Rigidbody>().AddForce(-other.transform.forward * this.repelForce);
        }
    }


    public static Vector3 GetGravity(Vector3 position, out Vector3 upAxis)
    {
        Vector3 up = position.normalized;
        upAxis = Physics.gravity.y < 0f ? up : -up;
        return up * Physics.gravity.y;
    }

    public static Vector3 GetUpAxis(Vector3 position)
    {
        Vector3 up = position.normalized;
        return Physics.gravity.y < 0f ? up : -up;
    }
}
