﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealPlayer : MonoBehaviour
{
    public SO_Player player;
    private GameObject playerManager;
    private bool cooldown;
    private readonly float cooldow = 0.01f;
    private readonly float healing = 1f;
    private void Awake()
    {
        playerManager = GameObject.Find("PlayerManager");
    }
    void Start()
    {
        cooldown = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /*
     * Mentres el pj esta dins del trigger el va recuperant vida.
     */
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && !cooldown)
        {
            if(player.hp < player.hpMax)
            {
                player.hp += this.healing;
                playerManager.GetComponent<HealthBarChange>().updateHealthBar();
                StartCoroutine(Cooldown());
            }else if(player.hp > player.hpMax)
            {
                player.hp = player.hpMax;
            }
        }
    }

    private IEnumerator Cooldown()
    {
        cooldown = true;
        yield return new WaitForSecondsRealtime(this.cooldow);
        cooldown = false;
    }
}
