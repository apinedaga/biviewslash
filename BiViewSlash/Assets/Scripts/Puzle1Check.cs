﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzle1Check : MonoBehaviour
{
    public SO_Puzle1 puzle;
    private Color originColor;
    private bool triggerActivated = false;
    public Camera camTrigger;
    public GameObject mainCam;
    public SO_PuzlesStatus pz;
    // Start is called before the first frame update
    void Start()
    {
        originColor = this.GetComponent<MeshRenderer>().material.color;
        if (pz.puzleDoor)
        {
            ActivatedTrigger();
        }
    }

    // Update is called once per frame



    public void ActivatedTrigger()
    {
        if (puzle.trigger)
        {
           
            triggerActivated = true;
            Time.timeScale = 0;
            mainCam.SetActive(false);
            camTrigger.gameObject.SetActive(true);
            StartCoroutine(PuzleCam(true));
        }
        else
        {
            
            triggerActivated = false;
            Time.timeScale = 0;
            mainCam.SetActive(false);
            camTrigger.gameObject.SetActive(true);
            StartCoroutine(PuzleCam(false));
        }
        
    }

    /*
     * Fixa la camera en la porta
     * Canvia el color del material del objecte
     * Desactiva la camera de la porta i torna activar la camera principal
     */
    IEnumerator PuzleCam(bool active)
    {
        yield return new WaitForSecondsRealtime(0.35f);
        if(active)
            this.GetComponent<MeshRenderer>().material.color = Color.green;
        else
            this.GetComponent<MeshRenderer>().material.color = originColor;
        yield return new WaitForSecondsRealtime(0.65f);
        camTrigger.gameObject.SetActive(false);
        mainCam.SetActive(true);
        Time.timeScale = 1;

    }


}
