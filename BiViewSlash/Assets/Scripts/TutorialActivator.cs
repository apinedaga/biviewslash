﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialActivator : MonoBehaviour
{
    [SerializeField] private TutorialPuzzleStatus status;
    private bool activated = false;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerHitBox"))
        {
            if (!this.activated)
            {
                this.GetComponent<MeshRenderer>().material.color = Color.green;
                this.activated = true;
                this.status.solved3 = true;
            }
            else
            {
                this.GetComponent<MeshRenderer>().material.color = Color.red;
                this.activated = false;
                this.status.solved3 = false;
            }
        }
    }
}
