﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public SO_Sound sound;
    // Start is called before the first frame update
    void Start()
    {
        
        if (sound.loaded)
        {
            GetComponent<AudioSource>().volume = sound.soundVolume;
        }
        else
        {
            GetComponent<AudioSource>().volume = sound.soundVolume;
        }
        GetComponent<AudioSource>().Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
