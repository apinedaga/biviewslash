﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obrePortaClauManager : MonoBehaviour
{
    [SerializeField] private GameObject[] myKeys;
    [SerializeField] private GameObject[] mySpheres;
    [SerializeField] private MiniPuzzleStatus myStatus;
    public SO_PuzlesStatus pStatus;

    private void Start()
    {

        if (!pStatus.puzleBossRight)
        {
            this.myStatus.solved1 = false;
            this.myStatus.solved2 = false;
        }
        if (this.myStatus.solved1 && this.myStatus.solved2) this.gameObject.SetActive(false);
        if (this.myStatus.solved1)
        {
            this.myKeys[0].SetActive(false);
            this.mySpheres[0].GetComponent<MeshRenderer>().material.color = Color.green;
        }
        if (this.myStatus.solved2)
        {
            this.myKeys[1].SetActive(false);
            this.mySpheres[1].GetComponent<MeshRenderer>().material.color = Color.green;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == this.myKeys[0].gameObject || other.gameObject == this.myKeys[1].gameObject)
        {
            print("e0");
            if (other.gameObject == this.myKeys[0].gameObject)
            {
                print("e1");
                this.myStatus.solved1 = true;
                this.myKeys[0].SetActive(false);
                this.mySpheres[0].GetComponent<MeshRenderer>().material.color = Color.green;
            } else if (other.gameObject == this.myKeys[1].gameObject)
            {
                print("e2");
                this.myStatus.solved2 = true;
                this.myKeys[1].SetActive(false);
                this.mySpheres[1].GetComponent<MeshRenderer>().material.color = Color.green;
            }
            if (this.myStatus.solved1 && this.myStatus.solved2)
            {
                this.gameObject.SetActive(false);
                pStatus.puzleBossRight = true;
            }
        }
    }
}
