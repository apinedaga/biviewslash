﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fadherTrigger : MonoBehaviour
{
    private bool triggered = false;
    private void OnTriggerEnter(Collider other)
    {
        if (!this.triggered && other.transform.tag == "Player")
        {
            this.triggered = true;
            this.GetComponentInParent<Rigidbody>().useGravity = true;
        }
    }
}
