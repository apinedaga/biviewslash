﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerHurtbox : MonoBehaviour
{
    
    void Start()
    {
        
    }

    
    void Update()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("EnemyHitBox") && !GetComponentInParent<setPlayerSO>().stats.invencible) {
            GetComponentInParent<Moviment>().playerStatus = Moviment.Status.ATTACKED;
            GetComponentInParent<setPlayerSO>().stats.invencible = true;
            StartCoroutine(EndInvencible());
        }  
    }

    IEnumerator EndInvencible()
    {
        //yield return new WaitForSecondsRealtime(0.25f);
        //GetComponentInParent<Moviment>().ChangeToStay();
        yield return new WaitForSecondsRealtime(0.25f);
        if (GetComponentInParent<setPlayerSO>().stats.invencible)
        {
            GetComponentInParent<setPlayerSO>().stats.invencible = false;
        }
           
        yield return new WaitForSecondsRealtime(0.25f);
    }
}
