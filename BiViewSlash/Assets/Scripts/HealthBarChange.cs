﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarChange : MonoBehaviour
{
    public Image hpBarImage;
    
    [Range(0, 1)]
    public float hpBarProgress = 0;

    public SO_Player stats;

    /*
     * Seteja la barra de vida a la vida que te el personatge
     */
    void Start()
    {
        if (stats.hp != stats.hpMax)
            hpBarProgress = 1 - (stats.hpMax - stats.hp)/ stats.hpMax;
        else
            hpBarProgress = 1;

        
        hpBarImage.fillAmount = hpBarProgress;
    }


    private void Update()
    {
        if (stats.hp <= 0)
            hpBarProgress = 0;

        if (stats.hp == stats.hpMax)
            hpBarProgress = 1;

        hpBarImage.fillAmount = hpBarProgress;
    }


    public void updateHealthBar()
    {
        if(stats.hp > 0)
        {
            hpBarProgress = 1 - (stats.hpMax - stats.hp) / stats.hpMax;
            hpBarImage.fillAmount = hpBarProgress;
        }
        else
        {
            hpBarImage.fillAmount = 0;
        }
        
    }

    
}
