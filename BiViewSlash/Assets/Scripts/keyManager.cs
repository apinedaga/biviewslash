﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class keyManager : MonoBehaviour
{
    [SerializeField] private GameObject doorToOpen;
    [SerializeField] private GameObject sphereToActivate;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == this.doorToOpen)
        {
            print("colisiono amb porta");
            sphereToActivate.gameObject.GetComponent<MeshRenderer>().material.color = Color.green;
        }
    }
}
