﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class cameraManager : MonoBehaviour
{
    public GameObject cameraCenital;
    public GameObject cameraLateral;
    public GameObject grayScaleCenital;
    public GameObject grayScaleLateral;
    private float cameraAnimationDistance = 30;
    private float realCamDistance = 70;
    private float sopTimeDelay = 0.15f;
    private float cameraSpeed = 8f;
    private float cameraSize = 13;
    [SerializeField] private float camLateralHeight;
    [SerializeField] private float camLateralDistance;
    public bool cenitalMode = true;
    private bool onCameraTransition = false;
    public CameraType camType;
    public GameObject objectManager;
    private bool settedAnimationCameraDistance = false;

    private bool zone1Minitutorial = false;

    private void Awake()
    {
        this.cameraCenital.transform.LookAt(this.transform.position);
        this.cameraLateral.transform.LookAt(this.transform.position);
        this.grayScaleCenital.gameObject.SetActive(false);
        this.grayScaleLateral.gameObject.SetActive(false);
        this.cameraCenital.GetComponent<Camera>().orthographic = true;
        this.cameraLateral.GetComponent<Camera>().orthographic = true;
        this.cameraCenital.GetComponent<Camera>().orthographicSize = this.cameraSize;
        this.cameraLateral.GetComponent<Camera>().orthographicSize = this.cameraSize;
        camType.cenital = true;


    }
    // Start is called before the first frame update
    void Start()
    {
        //camLateralDistance = 130f;
        this.canviaLaVista();
        this.cameraCenital.transform.LookAt(this.transform.position);
        this.cameraLateral.transform.LookAt(this.transform.position);
    }

    // Update is called once per frame
    void Update()
    {

        if (this.onCameraTransition)
        {
            changeCameraTransition();
        }
        else
        {
            if (!this.zone1Minitutorial) this.camerasFollowPj();
            if (this.zone1Minitutorial) this.camerasFollowPjMiniTutorial();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (SceneManager.GetActiveScene().name == "MinitutorialAutomatic")
        {
            if (other.transform.tag == "MinitutorialZone1")
            {
                this.zone1Minitutorial = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (SceneManager.GetActiveScene().name == "MinitutorialAutomatic")
        {
            if (other.transform.tag == "MinitutorialZone1")
            {
                this.zone1Minitutorial = false;
            }
        }
    }

    private void camerasFollowPjMiniTutorial()
    {
        if (this.cenitalMode)
        {
            Vector3 camPosition = new Vector3(this.transform.position.x, this.transform.position.y + this.realCamDistance, this.transform.position.z);
            this.cameraCenital.transform.position = camPosition;
            this.cameraLateral.transform.position = this.transform.position;
            this.cameraCenital.transform.LookAt(this.transform.position);
        }
        else
        {

            Vector3 camPosition = new Vector3(60, this.transform.position.y + camLateralHeight, this.transform.position.z);
            this.cameraLateral.transform.position = camPosition;
            this.cameraCenital.transform.position = this.transform.position;
            this.cameraLateral.transform.LookAt(camPosition);
            this.cameraLateral.gameObject.transform.eulerAngles = new Vector3(0, this.cameraLateral.gameObject.transform.eulerAngles.y, this.cameraLateral.gameObject.transform.eulerAngles.z);
        }
    }

    public void iniciarCanvi()
    {
        if (!this.onCameraTransition)
        {
            this.gameObject.GetComponent<Rigidbody>().velocity = new Vector3(0, this.gameObject.GetComponent<Rigidbody>().velocity.y, this.gameObject.GetComponent<Rigidbody>().velocity.z);
            Time.timeScale = 0;
            StartCoroutine(this.slowMoDelay());
            this.grayScaleCenital.gameObject.SetActive(true);
            this.grayScaleLateral.gameObject.SetActive(true);
            //objectManager.GetComponent<ObjectManager>().changePositionObjects(); //Canvies la posició dels objectes al fer el canvi de vista.
        }
    }

    private void canviaLaVista()
    {

        if (this.cenitalMode)
        {
            Vector3 targetCam2 = new Vector3(this.transform.position.x, this.transform.position.y + this.cameraAnimationDistance, this.transform.position.z);
            this.cameraCenital.transform.position = targetCam2;
            this.cameraCenital.SetActive(true);
            this.cameraLateral.SetActive(false);
            this.cameraLateral.transform.position = this.transform.position;

        }
        else
        {
            Vector3 targetCam2 = new Vector3(this.transform.position.x + this.cameraAnimationDistance, this.transform.position.y + this.camLateralHeight, this.transform.position.z);
            this.cameraLateral.transform.position = targetCam2;
            this.cameraCenital.SetActive(false);
            this.cameraLateral.SetActive(true);
            this.cameraCenital.transform.position = this.transform.position;
        }
    }

    private void changeCameraTransition()
    {
        //anar cap a cenital
        if (this.cenitalMode)
        {
            if (!this.settedAnimationCameraDistance)
            {
                Vector3 targetCam = new Vector3(this.transform.position.x, this.transform.position.y + this.cameraAnimationDistance, this.transform.position.z);
                this.cameraLateral.GetComponent<Camera>().transform.position = targetCam;
                this.cameraCenital.transform.position = this.transform.position;
                this.settedAnimationCameraDistance = true;
                GameEvents.canviDeVista.CenitalChangeEvent();
            }
            this.cameraLateral.transform.position = Vector3.MoveTowards(this.cameraLateral.transform.position, this.transform.position, this.cameraSpeed);
            this.cameraCenital.GetComponent<Camera>().orthographic = false;
            this.cameraLateral.GetComponent<Camera>().orthographic = false;
            if (this.cameraLateral.transform.position == this.transform.position)
            {
                objectManager.GetComponent<ObjectManager>().changePositionObjects(); //Canvies la posició dels objectes al fer el canvi de vista.
                objectManager.GetComponent<ObjectManager>().ChangeObjectActivation();
                objectManager.GetComponent<ObjectManager>().EnemyActivation();
                objectManager.GetComponent<ObjectManager>().RespawnActivation();
                this.cameraLateral.SetActive(false);
                this.cameraCenital.SetActive(true);
                Vector3 targetCam2 = new Vector3(this.transform.position.x, this.transform.position.y + this.cameraAnimationDistance, this.transform.position.z);
                this.cameraCenital.transform.position = Vector3.MoveTowards(this.cameraCenital.transform.position, targetCam2, this.cameraSpeed);
                if (this.cameraCenital.transform.position == targetCam2)
                {
                    this.onCameraTransition = false;
                    this.settedAnimationCameraDistance = false;
                    this.cameraCenital.GetComponent<Camera>().orthographic = true;
                    this.cameraLateral.GetComponent<Camera>().orthographic = true;
                    Vector3 targetCam3 = new Vector3(this.transform.position.x, this.transform.position.y + this.realCamDistance, this.transform.position.z);
                    this.cameraCenital.GetComponent<Camera>().transform.position = targetCam3;
                    StartCoroutine(this.slowMoWait());
                }
                this.cameraCenital.transform.LookAt(this.transform.position);
            }
        }
        else // anar cap a lateral
        {
            if (!this.settedAnimationCameraDistance)
            {
                Vector3 targetCam = new Vector3(this.transform.position.x, this.transform.position.y + this.cameraAnimationDistance, this.transform.position.z);
                this.cameraCenital.GetComponent<Camera>().transform.position = targetCam;
                this.settedAnimationCameraDistance = true;
                GameEvents.canviDeVista.LateralChangeEvent();

            }
            this.cameraCenital.transform.position = Vector3.MoveTowards(this.cameraCenital.transform.position, this.transform.position, this.cameraSpeed);
            this.cameraCenital.GetComponent<Camera>().orthographic = false;
            this.cameraLateral.GetComponent<Camera>().orthographic = false;
            if (this.cameraCenital.transform.position == this.transform.position)
            {
                objectManager.GetComponent<ObjectManager>().changePositionObjects(); //Canvies la posició dels objectes al fer el canvi de vista.
                objectManager.GetComponent<ObjectManager>().ChangeObjectActivation();
                objectManager.GetComponent<ObjectManager>().EnemyActivation();
                objectManager.GetComponent<ObjectManager>().RespawnActivation();
                this.cameraCenital.SetActive(false);
                this.cameraLateral.SetActive(true);
                Vector3 targetCam2 = new Vector3(this.transform.position.x + this.cameraAnimationDistance, this.transform.position.y + this.camLateralHeight, this.transform.position.z);

                this.cameraLateral.transform.position = Vector3.MoveTowards(this.cameraLateral.transform.position, targetCam2, this.cameraSpeed);
                if (this.cameraLateral.transform.position == targetCam2)
                {
                    this.onCameraTransition = false;
                    this.settedAnimationCameraDistance = false;
                    this.cameraCenital.GetComponent<Camera>().orthographic = true;
                    this.cameraLateral.GetComponent<Camera>().orthographic = true;
                    Vector3 targetCam3 = new Vector3(this.transform.position.x + this.camLateralDistance, this.transform.position.y, this.transform.position.z);
                    this.cameraCenital.GetComponent<Camera>().transform.position = targetCam3;
                    StartCoroutine(this.slowMoWait());
                }
                this.cameraLateral.transform.LookAt(this.transform.position);

            }
        }

    }

    private void camerasFollowPj()
    {
        if (this.cenitalMode)
        {
            Vector3 camPosition = new Vector3(this.transform.position.x, this.transform.position.y + this.realCamDistance, this.transform.position.z);
            this.cameraCenital.transform.position = camPosition;
            this.cameraLateral.transform.position = this.transform.position;
            this.cameraCenital.transform.LookAt(this.transform.position);
        }
        else
        {

            Vector3 camPosition = new Vector3(this.transform.position.x + this.camLateralDistance, this.transform.position.y + camLateralHeight, this.transform.position.z);
            this.cameraLateral.transform.position = camPosition;
            this.cameraCenital.transform.position = this.transform.position;
            this.cameraLateral.transform.LookAt(camPosition);
            this.cameraLateral.gameObject.transform.eulerAngles = new Vector3(0, this.cameraLateral.gameObject.transform.eulerAngles.y, this.cameraLateral.gameObject.transform.eulerAngles.z);
        }
    }

    private IEnumerator slowMoWait()
    {
        yield return new WaitForSecondsRealtime(this.sopTimeDelay);
        Time.timeScale = 1f;
        this.grayScaleCenital.gameObject.SetActive(false);
        this.grayScaleLateral.gameObject.SetActive(false);
    }
    private IEnumerator slowMoDelay()
    {
        yield return new WaitForSecondsRealtime(this.sopTimeDelay);
        this.onCameraTransition = true;
        if (this.cenitalMode)
        {
            this.cenitalMode = false;
        }
        else
        {
            this.cenitalMode = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "autoCenital" && !this.cenitalMode)
        {
            camType.cenital = true;
            this.iniciarCanvi();
        }
        else if (other.transform.tag == "autoLateral" && this.cenitalMode)
        {
            camType.cenital = false;
            this.iniciarCanvi();
        }
    }

    public static void activeGrayscaleSlowmo(float _slowMoTime, float _timeScale)
    {
        cameraManager.activeGrayscaleSlowmoCorroutine(_slowMoTime, _timeScale);
    }

    private static IEnumerator activeGrayscaleSlowmoCorroutine(float _slowMoTime, float _timeScale)
    {
        Time.timeScale = _timeScale;
        yield return new WaitForSecondsRealtime(_slowMoTime);
        Time.timeScale = 1;
    }

    public void ChangeCameraType()
    {
        if (camType.cenital)
            camType.cenital = false;
        else
            camType.cenital = true;
    }
}
