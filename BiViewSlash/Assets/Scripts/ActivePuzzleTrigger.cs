﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivePuzzleTrigger : MonoBehaviour
{
    public SO_Puzle1 pTrigger;
    public GameObject puzleManager;
    public GameObject spherePuzle;
    // Start is called before the first frame update
    void Start()
    {
        //pTrigger.trigger = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("PlayerHitBox"))
        {
            if (!pTrigger.trigger)
            {
                
                GetComponent<MeshRenderer>().material.color = Color.green;
                pTrigger.trigger = true;
                this.spherePuzle.GetComponent<Puzle1Check>().ActivatedTrigger();
            }
            else
            {
                
                GetComponent<MeshRenderer>().material.color = Color.red;
                pTrigger.trigger = false;
                this.spherePuzle.GetComponent<Puzle1Check>().ActivatedTrigger();
            }
            puzleManager.GetComponent<PuzzleManager>().puzle1Resolution();
        }
    }
}
