﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setPlayerSO : MonoBehaviour
{
    public SO_Player stats;

    private void Update()
    {
        this.stats.posicio = this.transform.position;
        this.stats.onCenitalMode = this.gameObject.GetComponent<cameraManager>().cenitalMode;
    }
}
