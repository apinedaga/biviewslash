﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHurtBox : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerHitBox"))
        {
            this.GetComponentInParent<EnemyAttack>().ReciveDamage(other.GetComponent<attackHitBox>().stats.attack);
        }

        if (other.CompareTag("PlayerHitBox2"))
        {
            this.GetComponentInParent<EnemyAttack>().ReciveDamage(other.GetComponent<attackHitBox>().stats.attack+20);
        }

        if (other.CompareTag("Fire"))
        {
            print("FIRE");
            this.GetComponentInParent<EnemyAttack>().ReciveDamage(70);
        }
    }
}
