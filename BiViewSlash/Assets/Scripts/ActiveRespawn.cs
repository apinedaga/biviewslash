﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveRespawn : MonoBehaviour
{
    public SO_LastRespawn respawnPos;
    public SO_respawn respawn;
    public SO_RespawnsStatus stR;
    // Start is called before the first frame update
    void Start()
    {
        if (respawn.nameRespawn == "respawnFirstDoor" && stR.respawnFirstDoor)
        {
            respawn.respawnActive = true;
        }
        if (respawn.nameRespawn == "respawnBossDoor" && stR.respawnBossDoor)
        {
            respawn.respawnActive = true;
        }
        if (respawn.nameRespawn == "respawnBossFloor" && stR.respawnBossFloor)
        {
            respawn.respawnActive = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            respawnPos.lastRespawn = new Vector3(transform.position.x - 1, transform.position.y, transform.position.z - 4);
            respawn.respawnActive = true;
            this.gameObject.SetActive(false);
            if(respawn.nameRespawn == "respawnFirstDoor")
            {
                stR.respawnFirstDoor = true;
            }
            if (respawn.nameRespawn == "respawnBossDoor")
            {
                stR.respawnBossDoor = true;
            }
            if (respawn.nameRespawn == "respawnBossFloor")
            {
                stR.respawnBossFloor = true;
            }
        }
    }
}
