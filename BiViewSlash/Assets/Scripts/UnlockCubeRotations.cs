﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockCubeRotations : MonoBehaviour
{
    [SerializeField] private GameObject activationCanvas;
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<Rigidbody>().angularVelocity = new Vector3(1f, 1.5f, 0.75f);  
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            this.activationCanvas.GetComponent<unlockedViewsCanvas>().appearingCanvas();
            other.gameObject.GetComponent<Moviment>().unlockChange();
            Destroy(this.gameObject);
        }
    }
}
