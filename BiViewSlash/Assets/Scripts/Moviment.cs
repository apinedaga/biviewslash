﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Moviment : MonoBehaviour
{
    private bool lockedChange = true;
    public float speed;
    public float airMove;
    private bool Grounded = true;
    Rigidbody rigidbody;
    //private Actions act;
    private PlayerAction act; //Script per controlar el canvi d'animació
    public SO_LastRespawn respawn;
    private GameObject obj;
    public SO_Player player;


    public enum Status { NONE, ATTACKED, IDLE, JUMP, RUNNING }; //Controla el estat en que es trova el pj
    public Status playerStatus;

    private void Awake()
    {
        this.rigidbody = this.gameObject.GetComponent<Rigidbody>();
        //act = gameObject.GetComponent<Actions>();
        act = gameObject.GetComponent<PlayerAction>();
        playerStatus = Status.IDLE;
        //GetComponent<PlayerController>().SetArsenal("Cylinder");
        respawn.lastRespawn = this.transform.position; // Posa l'última posició de respawn a la posició inicial de l'escena

        if (SceneManager.GetActiveScene().name != "MinitutorialAutomatic")
        {
            this.lockedChange = false;
        }
        player.cinematic = false;
    }
    
    void Start()
    {
        Grounded = true;
        if (player.loaded)
        {
            transform.position = player.savedPosition;
            player.loaded = false;
        }
    }

    public void unlockChange()
    {
        this.lockedChange = false;
    }

    
    void Update()
    {
       // print(Input.GetAxisRaw("viewChange"));

        if (this.Grounded && Time.timeScale > 0.5)
        {
            girarPj();
        }
        if ((Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Joystick1Button1) /*|| Gamepad.current[GamepadButton.Y]*/) && !this.lockedChange)
        {
            this.gameObject.GetComponent<cameraManager>().ChangeCameraType();
            this.gameObject.GetComponent<cameraManager>().iniciarCanvi();
        }else if (Input.GetAxisRaw("viewChange") == 0)
        {
            
        }
        if (this.Grounded && !player.cinematic)
        {
            if (this.gameObject.GetComponent<cameraManager>().cenitalMode)
            {
                movimentCenital();
            }
            else
            {
                movimentLateral();
            }
        }
        else
        {
            if (this.gameObject.GetComponent<cameraManager>().cenitalMode)
            {
                //airMovimentCenital();
            }
            else
            {
                //airMovimentLateral();
            }
        }

        checkAnimation();

        if (transform.position.y < -5)
            respawning();

        if (player.hp <= 0)
        {
            StartCoroutine(DeathPlayer());
            
            
        }
            

    }

    IEnumerator DeathPlayer()
    {
        player.invencible = true;
        act.Death();
        yield return new WaitForSecondsRealtime(2.5f);
        act.DeathOff();
        player.hp = player.hpMax;
        respawning();
    }

    private void girarPj()
    {
        if (this.gameObject.GetComponent<cameraManager>().cenitalMode)
        {
            float moveVertical = Input.GetAxis("Vertical");
            float moveHorizontal = Input.GetAxis("Horizontal");
            if (Input.GetAxis("dpadRL") < -0.5 || Input.GetAxis("dpadRL") > 0.5)
            {
                moveHorizontal = Input.GetAxis("dpadRL");
            }
            if (Input.GetAxis("dpadUpDown") < -0.5 || Input.GetAxis("dpadUpDown") > 0.5)
            {
                moveVertical = Input.GetAxis("dpadUpDown");
            }
            Vector3 newPosition = new Vector3(moveHorizontal, 0, moveVertical);
            transform.LookAt(newPosition + transform.position);
        } else
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            if (Input.GetAxis("dpadRL") < -0.5 || Input.GetAxis("dpadRL") > 0.5)
            {
                moveHorizontal = Input.GetAxis("dpadRL");
            }
            if (moveHorizontal > 0)
            {
                moveHorizontal = 1;
            } else if (moveHorizontal < 0)
            {
                moveHorizontal = -1;
            }
            

            Vector3 newPosition = new Vector3(0, 0, moveHorizontal);
            transform.LookAt(newPosition + this.transform.position);
        }
        
    }

    void movimentCenital()
    {
        float movHoritzontal = Input.GetAxis("Horizontal");
        float movVertical = Input.GetAxis("Vertical");
        if (Input.GetAxis("dpadRL") < -0.5 || Input.GetAxis("dpadRL") > 0.5)
        {
            movHoritzontal = Input.GetAxis("dpadRL");
        }
        if (Input.GetAxis("dpadUpDown") < -0.5 || Input.GetAxis("dpadUpDown") > 0.5)
        {
            movVertical = Input.GetAxis("dpadUpDown");
        }
        if (movHoritzontal > 0.5f || movVertical > 0.5f || movHoritzontal < -0.5f || movVertical < -0.5f)
        {
            this.rigidbody.velocity = new Vector3(movHoritzontal * speed
            * Time.deltaTime, this.rigidbody.velocity.y, movVertical * speed * Time.deltaTime);
        }else
        {
            this.rigidbody.velocity = new Vector3(0 * speed
           * Time.deltaTime, this.rigidbody.velocity.y, 0);
        }
    }

    void movimentLateral()
    {
        float movHoritzontal = Input.GetAxis("Horizontal");
        if (Input.GetAxis("dpadRL") < -0.5 || Input.GetAxis("dpadRL") > 0.5)
        {
            movHoritzontal = Input.GetAxis("dpadRL");
        }
        if (movHoritzontal > 0.5f || movHoritzontal < -0.5f)
        {
            this.rigidbody.velocity = new Vector3(0, this.rigidbody.velocity.y, movHoritzontal * speed * Time.deltaTime);
        }
        else
        {
            this.rigidbody.velocity = new Vector3(0, this.rigidbody.velocity.y, 0);
        }
    }

    void airMovimentCenital()
    {
        this.rigidbody.AddForce(new Vector3(Input.GetAxis("Horizontal")*this.airMove, 0, Input.GetAxis("Vertical")*this.airMove));
    }

    void airMovimentLateral()
    {
        
        this.rigidbody.AddForce(new Vector3(0, 0, Input.GetAxis("Horizontal")*this.airMove));
    }

    public void setGrounded(bool b)
    {
        this.Grounded = b;
        act.Stay();
    }

    /*
     * Comprova si el pj s'està movent.
     *  -En cas que si canvia l'animació i l'estat a run
     *  -No, la posa a idle (Stay())
     */
    public void checkAnimation()
    {

        if(Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0 && Input.GetAxis("dpadRL") == 0 && Input.GetAxis("dpadUpDown") == 0)
        {
            act.Stay();
            playerStatus = Status.IDLE;
        }
        else if(Input.GetAxis("Horizontal") > 0.5f || Input.GetAxis("Vertical") > 0.5f || Input.GetAxis("Horizontal") < -0.5f || Input.GetAxis("Vertical") < -0.5f)
        {
            playerStatus = Status.RUNNING;
            act.Run();
        }
        else if (Input.GetAxis("dpadRL") > 0.5f || Input.GetAxis("dpadUpDown") > 0.5f || Input.GetAxis("dpadRL") < -0.5f || Input.GetAxis("dpadUpDown") < -0.5f)
        {
            playerStatus = Status.RUNNING;
            act.Run();
        }
    }

    /*
     * Si el personatge mor o es cau, torna a l'ultima pos de respawn
     *  Zona Respawn pot ser, pos inicial
     *  El ultim checkpoint trovat.
     */
    public void respawning()
    {
        transform.position = respawn.lastRespawn;
        player.invencible = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            act.Stay();
        }
        //Comprova si colisiona amb uns dels objectes que canvia segons la vista.
        //Per fer que el pj també es mogui amb el pj, el fa pare.
        if (collision.gameObject.CompareTag("ChangePosObject"))
        {
            obj = new GameObject(); //Objecte intermedi per evitar que es deformi la geometria del pj
            obj.transform.SetParent(collision.gameObject.transform); //fas el obj el fill del objecte colisionat
            obj.transform.position = collision.transform.position;
            transform.SetParent(obj.transform); //fas el pj fill del obj creat.
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        //Si es una plataforma que es mou fas el pj filla d'ella.
        if (collision.gameObject.CompareTag("MovingPlatform"))
        {
            this.transform.parent = collision.transform;
        }
    }


    private void OnCollisionExit(Collision collision)
    {
        //Un cop surts de les colision dels objectes que es mouen sets el pare a null i destrueixes
        //l'objecte creat.
        if (collision.gameObject.CompareTag("ChangePosObject"))
        {
            this.transform.SetParent(null);
            Destroy(obj.gameObject);
        }

        if (collision.gameObject.CompareTag("MovingPlatform"))
        {
            this.transform.SetParent(null);
        }
    }
}