﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActivation : MonoBehaviour
{
    public EnemyActivationEvent enemyEvent;
    public SO_TriggersEnemies t;
    public SO_TriggersEnemies.triggerPos area;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetActive()
    {
        this.transform.GetChild(5).GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        this.transform.GetChild(5).GetChild(1).GetComponent<MeshRenderer>().enabled = true;

    }

    public void SetInactive()
    {
        if (area != t.currentArea)
        {
            this.transform.GetChild(5).GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            this.transform.GetChild(5).GetChild(1).GetComponent<MeshRenderer>().enabled = false;
        }

    }
}
