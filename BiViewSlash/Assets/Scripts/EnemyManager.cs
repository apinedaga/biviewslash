﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public SO_Player player;
    public SO_DeadEnemies deadEnemies;
    // Start is called before the first frame update
    void Start()
    {
        /*
         * Al carregar el joc d'un fitxer guardat, comprova els enemics que han estat destruits i els desactiva al començar l'escena.
         */
        if (deadEnemies.loaded)
        {
            int count = transform.childCount;
            deadEnemies.loaded = false;
            foreach (string item in deadEnemies.deadEnemies)
            {
                for (int i = 0; i < count; i++)
                {
                    if (transform.GetChild(i).name == item)
                        transform.GetChild(i).gameObject.SetActive(false);
                }
            }
        }
    }
}
