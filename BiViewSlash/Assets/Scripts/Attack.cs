﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    int hit;
    public SO_AttackMele[] atkMele;
    enum attackStatus {NONE, FIRST, SECOND}; //estatus del combo
    attackStatus currentAttack;
    PlayerAction act;
    public SO_Player stats;
    [SerializeField] int child; //Posicio de la hitbox com a filla.
    public GameObject fireWave;
    public GameObject weapon;
    public CameraType cam;
    public bool ultiActivated = false;
    // Start is called before the first frame update
    void Start()
    {
        act = GetComponentInParent<PlayerAction>();
        hit = 0;
        currentAttack = attackStatus.NONE;

    }

    // Update is called once per frame
    void Update()
    {
        //si pulses la tecla i el status es none, fas el primer atac
        if ((Input.GetKeyDown(KeyCode.K) || Input.GetKeyDown(KeyCode.Joystick1Button2)) && (currentAttack == attackStatus.NONE))
        {
            currentAttack = attackStatus.FIRST;
            StartCoroutine(Attacking(atkMele[0], 1f));
            
        //Si tornes a pulsar i el current attack es 1 fas el segon
        }else if ((Input.GetKeyDown(KeyCode.K) || Input.GetKeyDown(KeyCode.Joystick1Button2)) && (currentAttack == attackStatus.FIRST))
        {
            currentAttack = attackStatus.SECOND;
        }

        if ((Input.GetKeyDown(KeyCode.L) || Input.GetKeyDown(KeyCode.Joystick1Button3)) && stats.mp >= 30 && !ultiActivated)
        {
            
            StartCoroutine(Ulti());
            ultiActivated = true;
        }

    }


    IEnumerator Attacking(SO_AttackMele move, float time)
    {
        
        //Primer atac, fas l'animació del primer atac
        if (currentAttack == attackStatus.FIRST)
        {
            act.Attack();
            //this.transform.GetChild(7).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material.color = Color.red;

        }
        yield return new WaitForSecondsRealtime(0.75f);

        //Segon atac, fas l'animació del segon atac
        if(currentAttack == attackStatus.SECOND)
        {
            act.Combo1(); //actives l'animació
            //this.transform.GetChild(7).GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material.color = Color.blue;
            yield return new WaitForSecondsRealtime(0.15f); //esperes per evitar que es canvii a l'animacio idle
            act.AttackOff(); //poses l'animació del primer atac a false
            
           
            yield return new WaitForSecondsRealtime(0.55f);
            act.Combo1Off(); //acabes l'animació del segon atac.
            
        }
        yield return new WaitForSecondsRealtime(0.25f);
        act.AttackOff();//poses l'animació del primer atack a false (Per si no fas el segon atac)
        yield return new WaitForSecondsRealtime(0.5f);

        currentAttack = attackStatus.NONE;
        
        //this.transform.GetChild(child).GetChild(0).gameObject.SetActive(false);
        
        act.Stay();
        

    }

    IEnumerator Ulti()
    {
        act.Ulti();
       
        yield return new WaitForSecondsRealtime(1.35f);
        /*
         * L'atac a distancia es un efecte de particles.
         * Instancies el prefab a la posició del personatge i el rotes segons on estigui mirant el personatge, per fer que l'atac es mogui en aquesta direcció.
         */
        GameObject fw = Instantiate(fireWave);
        fw.transform.position = new Vector3(weapon.transform.position.x, weapon.transform.position.y, weapon.transform.position.z);
        fw.transform.rotation = this.transform.rotation;
        if (!cam.cenital)
        {
            //fw.transform.Rotate(new Vector3(fw.transform.rotation.x, fw.transform.rotation.y, 90));
            fw.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.VerticalBillboard;
        }
        else
        {
            
            fw.GetComponent<ParticleSystemRenderer>().renderMode = ParticleSystemRenderMode.HorizontalBillboard;
        }
            
        fw.transform.GetChild(0).GetComponent<movingCol>().setPos(fw.transform.position);
        fw.transform.GetChild(0).GetComponent<movingCol>().setRot(fw.transform.rotation);
        //fireWave.SetActive(true);
        yield return new WaitForSecondsRealtime(1f);
        act.UltiOff();
        //fireWave.gameObject.SetActive(false);
        //Destroy(fw);
        //stats.mp -= 50;
        stats.mp -= 30;
        if (stats.mp < 0)
            stats.mp = 0;
        yield return new WaitForSecondsRealtime(1f);
        ultiActivated = false;
    }
}
