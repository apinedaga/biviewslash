﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class agafaObjecte : MonoBehaviour
{
    private GameObject objectToPick;
    private GameObject obtainedObject;
    private bool objectPicked = false;
    private bool canPickObject = false;
    private void OnTriggerEnter(Collider other)
    {
        if ((other.transform.tag == "RedPuzzleCube" || other.transform.tag == "BluePuzzleCube" || other.transform.tag ==  "key") && this.obtainedObject == null)
        {
            this.canPickObject = true;
            this.objectToPick = other.gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if ((other.transform.tag == "RedPuzzleCube" || other.transform.tag == "BluePuzzleCube" || other.transform.tag == "key") && this.obtainedObject == null)
        {
            this.canPickObject = false;
            this.objectToPick = null;
        }
    }

    private void Update()
    {
        if (this.canPickObject && !this.objectPicked)
        {
            if (Input.GetKeyDown(KeyCode.F) || Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                this.objectPicked = true;
                this.obtainedObject = this.objectToPick;
                this.obtainedObject.GetComponent<MeshRenderer>().enabled = false;
                this.obtainedObject.GetComponent<SphereCollider>().enabled = false;
            }
        } else if (this.objectPicked)
        {
            if (Input.GetKeyDown(KeyCode.F) || Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                this.objectPicked = false;
                this.obtainedObject.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 1, this.transform.position.z);
                this.obtainedObject.GetComponent<MeshRenderer>().enabled = true;
                this.obtainedObject.GetComponent<SphereCollider>().enabled = true;
                this.obtainedObject = null;
            }
        }
    }
}
