﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPuzleDoor : MonoBehaviour
{
    public BossPuzleStatus myStatus;
    [SerializeField] private GameObject[] mySpheres;
    /*
     * comprova el estat en el que es troben la porta.
     * Si les dues espheres estan activades, canvia el color a verd i es desactiva la porta.
     * Es comprova en un Scriptable Object
     */
    private void Awake()
    {
        if (this.myStatus.rightSprereActive)
        {
            this.mySpheres[0].GetComponent<MeshRenderer>().material.color = Color.green;
        }
        if (this.myStatus.leftSphereActive)
        {
            this.mySpheres[1].GetComponent<MeshRenderer>().material.color = Color.green;
        }
        if (this.myStatus.rightSprereActive && this.myStatus.leftSphereActive)
        {
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }
    /*
     * Si les dues esferes estan activades, desactiva la porta.
     */
    public void checkPuzleStatus()
   {
        if (this.myStatus.rightSprereActive && this.myStatus.leftSphereActive)
        {
            this.gameObject.GetComponent<MeshRenderer>().enabled = false;
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
        }
   }

    public void SetRightSphere()
    {
       if (this.myStatus.rightSprereActive)
        {
            this.mySpheres[0].GetComponent<MeshRenderer>().material.color = Color.green;
        } else
        {
            this.mySpheres[0].GetComponent<MeshRenderer>().material.color = Color.yellow;
        }
        this.checkPuzleStatus();
    }

    public void SetLeftSphere()
    {
       if (this.myStatus.leftSphereActive)
        {
            this.mySpheres[1].GetComponent<MeshRenderer>().material.color = Color.green;
        }
        else
        {
            this.mySpheres[1].GetComponent<MeshRenderer>().material.color = Color.yellow;
        }
        this.checkPuzleStatus();
    }
}
