﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyinTime : MonoBehaviour
{
    private float lifeExplosionTime = 0.5f;
    private void Start()
    {
        StartCoroutine(this.destroyTime());
    }

    private IEnumerator destroyTime()
    {
        yield return new WaitForSecondsRealtime(this.lifeExplosionTime);
        Destroy(this.gameObject);
    }
}
