﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followPj : MonoBehaviour
{
    public SO_Player playerStats;
    private bool playerInRange = false;
    private float speed = 1;
    private Vector3 attackTaget;
    private bool onEnemyZone = true;
    private Vector3 myZone;
    public bool mustStop = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if (!this.onEnemyZone)
       {
            this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(this.myZone.x, this.transform.position.y, this.myZone.z), this.speed * Time.deltaTime);
       }
    }

    public void setIsInRange(bool b)
    {
        this.playerInRange = b;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "enemyZone")
        {
            this.myZone = other.transform.position;
            if (this.playerInRange && !mustStop)
            {
                this.transform.LookAt(new Vector3(this.playerStats.posicio.x, this.transform.position.y, this.playerStats.posicio.z));
                this.transform.position = Vector3.MoveTowards(this.transform.position, new Vector3(this.playerStats.posicio.x, this.transform.position.y, this.playerStats.posicio.z), this.speed * Time.deltaTime);
            } else if (this.playerInRange)
            {
                this.transform.LookAt(new Vector3(this.playerStats.posicio.x, this.transform.position.y, this.playerStats.posicio.z));
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "enemyZone")
        {
            this.onEnemyZone = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "enemyZone")
        {
            this.onEnemyZone = true;
        }
    }

}
