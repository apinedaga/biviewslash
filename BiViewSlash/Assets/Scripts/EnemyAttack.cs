﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    private bool attacking;
    public SO_EnemyStats stats;
    public float hp;
    public SO_Player playerStatus;
    public HpManagerEvent hpEvent;
    private Animator animator;
    enum status{ATTACKING, READY, NONE};
    status attackStatus;
    
    void Start()
    {
        animator = this.GetComponent<Animator>();
        attacking = false;
        attackStatus = status.NONE;
        
        this.hp = this.stats.maxHp;
    }

    
    void Update()
    {
        
        if (attacking && (this.attackStatus == status.READY))
        {
            
            this.attackStatus = status.ATTACKING;
            basicAttack();
            StartCoroutine(this.cooldown());
            
        }
        CheckAnimation();
        
    }

    private void CheckAnimation()
    {
        if (attackStatus == status.ATTACKING)
        {
            animator.SetBool("Attacking", true);
        }
        else
        {
            animator.SetBool("Attacking", false);
        }
    }

    public void setAttacking(bool attacking)
    {
        this.attacking = attacking;
        if(attacking)
            this.attackStatus = status.READY;
        else
            this.attackStatus = status.NONE;
    }

    IEnumerator cooldown()
    {
        yield return new WaitForSecondsRealtime(2f);
        this.attackStatus = status.READY;
    }

    public void basicAttack()
    {
        if (!playerStatus.invencible)
        {
            hpEvent.RaiseItem((float)this.stats.attack); 
        }
            
    }

    public void ReciveDamage(float dmg)
    {
        this.hp -= dmg;
        if(this.hp<= 0)
        {
            this.GetComponent<DeathEnemy>().death();
        }
        print(this.hp);
    }
}
