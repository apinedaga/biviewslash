﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class attackHitBox : MonoBehaviour
{
    public SO_Player stats;
    public GameObject hitEffect;
    
    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("EnemyHurtBox"))
        {
            if (stats.mp < stats.mpMax)
                stats.mp += 5;
            GameObject hit = Instantiate(hitEffect);
            hit.transform.position = other.transform.position;
        }
    }

    IEnumerator ActivateAgent(Collider other, NavMeshAgent agent)
    {
        yield return new WaitForSecondsRealtime(5f);
        agent.GetComponent<NavMeshAgent>().enabled = true;
        other.GetComponent<EnemyNavmesh>().detected = true;
    }
}
