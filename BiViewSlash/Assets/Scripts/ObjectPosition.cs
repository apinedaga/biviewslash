﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPosition : MonoBehaviour
{
    [SerializeField] private Vector3 cenitalPos;
    [SerializeField] private Vector3 lateralPos;
    public ObjectManagerEvent objectEvent;
    // Start is called before the first frame update
    private void Awake()
    {
        cenitalPos = this.transform.localPosition;
        //lateralPos = new Vector3(1.59f, -21.45f, -22.118f);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setCenitalPosition()
    {
        this.transform.localPosition = cenitalPos;
    }

    public void setLateralPosition()
    {
        this.transform.localPosition = lateralPos;
    }
}
