﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDetector : MonoBehaviour
{
    [SerializeField] private TutorialPuzzleStatus myStatus;
    private void FixedUpdate()
    {
        this.GetComponent<MeshRenderer>().material.color = Color.yellow;
        this.myStatus.solved1 = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if (this.transform.tag == "BluePuzzlePlatform")
        {

            if (other.transform.tag == "RedPuzzleCube")
            {
                this.GetComponent<MeshRenderer>().material.color = Color.red;
                this.myStatus.solved1 = false;
            }
            else if (other.transform.tag == "BluePuzzleCube")
            {
                this.GetComponent<MeshRenderer>().material.color = Color.green;
                this.myStatus.solved1 = true;
            }
        }
    }
}
