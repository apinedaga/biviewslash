﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyNavmesh : MonoBehaviour
{
    public GameObject player;
    public bool detected;
    NavMeshAgent navmesh;
    public CameraType cam;

    private bool playerInRange = false;
    private float speed = 1;
    private Vector3 attackTaget;
    private bool onEnemyZone = true;
    private Vector3 myZone;
    public bool mustStop = false;
    public GameObject obj;
    private GameObject hitBoxCenital;
    private GameObject hitBoxLateral;
    private Vector3 initialpos;

    private void Awake()
    {
        initialpos = this.transform.position;
        print(initialpos);
    }
    void Start()
    {
        this.navmesh = this.GetComponent<NavMeshAgent>();
        detected = false;
        this.hitBoxCenital = this.transform.GetChild(2).gameObject;
        this.hitBoxLateral = this.transform.GetChild(3).gameObject;
        this.hitBoxLateral.SetActive(false);
        this.hitBoxCenital.SetActive(true);

        this.transform.position = this.initialpos;
    }


    void Update()
    {
        /*
         * Si a detectat al pj aquest es mou per la seva zona i segueix al personatge amb el NavMesh Surface definit.
         */
        if (detected && onEnemyZone)
        {
            if (cam.cenital)
            {
                //Seteja la destinació al pj.
                this.navmesh.destination = new Vector3(this.player.transform.position.x, this.transform.position.y, this.player.transform.position.z);

                this.transform.LookAt(player.transform.position);

                this.navmesh.baseOffset = 0;
                this.hitBoxCenital.SetActive(true);
                this.hitBoxLateral.SetActive(false);
            }
            else
            {
                
                this.navmesh.destination = new Vector3(this.player.transform.position.x, this.transform.position.y, this.player.transform.position.z);

                this.transform.LookAt(new Vector3(this.player.transform.position.x, this.transform.position.y, this.player.transform.position.z));

                this.navmesh.baseOffset = 4;

                this.hitBoxCenital.SetActive(false);
                this.hitBoxLateral.SetActive(true);
            }


        }
        else if(onEnemyZone)
        {
            //Seteja la destinació en la seva pos.
            this.navmesh.destination = this.transform.position;
            if (cam.cenital)
            {
                this.navmesh.baseOffset = 0;
                this.hitBoxCenital.SetActive(true);
                this.hitBoxLateral.SetActive(false);
            }
            else
            {
                this.navmesh.baseOffset = 4;
                this.hitBoxCenital.SetActive(false);
                this.hitBoxLateral.SetActive(true);
            }
        }
    }

    public void setDetected(bool detected)
    {
        this.detected = detected;
    }

    public void setIsInRange(bool b)
    {
        this.playerInRange = b;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "enemyZone")
        {
            this.onEnemyZone = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "enemyZone")
        {
            this.onEnemyZone = true;
        }
    }
}
