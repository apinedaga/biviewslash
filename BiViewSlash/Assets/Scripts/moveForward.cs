﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveForward : MonoBehaviour
{
    private float speed = 15f;
    private float lifeTimeSeconds = 10f;
    [SerializeField] private float damage = 5f;
    [SerializeField] private SO_Player player;
    [SerializeField] private HpManagerEvent hpEvent;

    private void Awake()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        Invoke("autoDestruccio", this.lifeTimeSeconds);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 forward = this.transform.TransformDirection(Vector3.forward);
        this.GetComponent<Rigidbody>().velocity = forward * this.speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player" || other.transform.tag == "Ground" || other.transform.tag == "Obstacle") 
        {
            if (other.transform.tag == "Player")
            {
                //this.player.hp -= this.damage;
                print("Des de moveForward" + this.player.hp);
                this.hpEvent.RaiseItem(this.damage);
            }
            Destroy(this.gameObject);
        }
    }

    private void autoDestruccio()
    {
        Destroy(this.gameObject);
    }


}
