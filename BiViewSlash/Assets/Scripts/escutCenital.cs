﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class escutCenital : MonoBehaviour
{
    private float heightChange = 2.5f;
    private float resetForceField = 300f;
    private float forceField;
    private float forceFieldIncrement = 30f;
    private void Awake()
    {
        this.forceField = this.resetForceField;
    }
    void Start()
    {
        GameEvents.canviDeVista.onCenitalChange += cenitalChange;
        GameEvents.canviDeVista.onLateralChange += lateralChange;
    }

    private void cenitalChange()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - this.heightChange, this.transform.position.z);
    }

    private void lateralChange()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + this.heightChange, this.transform.position.z);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            other.gameObject.GetComponent<Rigidbody>().AddForce(this.transform.forward*this.forceField);
            this.forceField += this.forceFieldIncrement;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            this.forceField = this.resetForceField;
        }
    }

    private void OnDestroy()
    {
        GameEvents.canviDeVista.onCenitalChange -= cenitalChange;
        GameEvents.canviDeVista.onLateralChange -= lateralChange;
    }
}
