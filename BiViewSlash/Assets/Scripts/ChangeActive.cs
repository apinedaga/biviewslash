﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeActive : MonoBehaviour
{
    public SO_respawn respawn;
    void Start()
    {
        if (respawn.respawnActive)
            GetComponent<MeshRenderer>().material.color = Color.green;
    }

    // Update is called once per frame
    void Update()
    {
        if(respawn.respawnActive)
            GetComponent<MeshRenderer>().material.color = Color.green;
    }
}
