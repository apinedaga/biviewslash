﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD_View : MonoBehaviour
{
    [SerializeField] private Camera cenital;
    [SerializeField] private Camera lateral;
    public CameraType cam;
    public GameObject canvas;
    // Start is called before the first frame update
    void Start()
    {
        if (cam.cenital)
        {
            canvas.GetComponent<Canvas>().worldCamera = cenital;
        }
        else
        {
            canvas.GetComponent<Canvas>().worldCamera = lateral;
            canvas.GetComponent<Canvas>().planeDistance = 11;
        }

    }
    /*
     * Canvia la pos del HUD segons la vista de camera.
     */
    void Update()
    {
        if (cam.cenital)
        {
            canvas.GetComponent<Canvas>().worldCamera = cenital;
        }
        else
        {
            canvas.GetComponent<Canvas>().worldCamera = lateral;
            canvas.GetComponent<Canvas>().planeDistance = 11;
        }
    }
}
