﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectActivation : MonoBehaviour
{
    public ObjectActivationEvent objectEvent;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetActive()
    {
        this.GetComponent<MeshRenderer>().enabled = true;
        //this.gameObject.SetActive(true);
    }

    public void SetInactive()
    {
        this.GetComponent<MeshRenderer>().enabled = false;
        //this.gameObject.SetActive(false);
    }
}
