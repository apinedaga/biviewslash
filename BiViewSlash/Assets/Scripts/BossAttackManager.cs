﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAttackManager : MonoBehaviour
{
    private bool attackingFase1 = false;
    private bool attackingFase2 = false;
    public SO_Player playerStatus;

    public GameObject fallingDelayBullet;
    [SerializeField] private float shootfase1spawnHeight;
    [SerializeField] private float delayBetweenShootFase1;
    [SerializeField] private float delayBetweenAttackFase1;
    [SerializeField] private int shootsPerAttackFase1;

    [SerializeField] private GameObject delayExplosionZone;
    [SerializeField]  private float fase2ExplosionTime = 2f;
    [SerializeField]  private float attackDelay = 1f;
    private GameObject explosion;
    private void Start()
    {
        
    }
    public void attackFase1()
    {
        if (!this.attackingFase1) StartCoroutine(this.attack1());
    }

    public void attackFase2()
    {
        if (!this.attackingFase2) StartCoroutine(this.attack2());
    }

    public void attackFase3()
    {
        if (!this.attackingFase1) StartCoroutine(this.attack1());
        if (!this.attackingFase2) StartCoroutine(this.attack2());
    }

    private IEnumerator attack1()
    {
        this.attackingFase1 = true;
        for (int i = 0; i < this.shootsPerAttackFase1; i++)
        {
            GameObject go = Instantiate(this.fallingDelayBullet);
            go.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + shootfase1spawnHeight, this.transform.position.z);
            go.gameObject.GetComponent<delayFallingShoot>().setMyTarget(this.playerStatus.posicio);
            yield return new WaitForSecondsRealtime(this.delayBetweenShootFase1);
        }
        yield return new WaitForSecondsRealtime(this.delayBetweenAttackFase1);
        this.attackingFase1 = false;
    }

    private IEnumerator attack2()
    {
        this.attackingFase2 = true;
        GameObject go = Instantiate(this.delayExplosionZone);
        go.transform.position = this.playerStatus.posicio;
        go.GetComponent<DelayExplosionZone>().setMyExplosionTime(this.fase2ExplosionTime);
        yield return new WaitForSecondsRealtime(this.fase2ExplosionTime);
        yield return new WaitForSecondsRealtime(this.attackDelay);
        this.attackingFase2 = false;
    }

}
