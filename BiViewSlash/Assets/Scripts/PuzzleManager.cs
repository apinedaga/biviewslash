﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    public List<SO_Puzle1> puzle1 = new List<SO_Puzle1>();
    public bool[] puzle1Status;
    public GameObject doorPuzle1;
    public SO_Player stats;
    public SO_PuzlesStatus pStatus;
    void Start()
    {
        if (pStatus.puzleDoor)
            PuzleSetActive();
        else
            PuzleReset();

        puzle1Resolution();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void puzle1Resolution()
    {
        foreach (SO_Puzle1 item in puzle1)
        {
            if (item.name == "Trigger1")
                puzle1Status[0] = item.trigger;

            if (item.name == "Trigger2")
                puzle1Status[1] = item.trigger;

            if (item.name == "Trigger3")
                puzle1Status[2] = item.trigger;

            if (item.name == "Trigger4")
                puzle1Status[3] = item.trigger;
        }

        if (!puzle1Status[0] && puzle1Status[1] && puzle1Status[2] && !puzle1Status[3])
        {
            StartCoroutine(PuzleDone());
        }
            
    }

    public void PuzleSetActive()
    {
        foreach (SO_Puzle1 item in puzle1)
        {
            if (item.name == "Trigger1")
                item.trigger = false;

            if (item.name == "Trigger2")
                item.trigger = true;

            if (item.name == "Trigger3")
                item.trigger = true;

            if (item.name == "Trigger4")
                item.trigger = false;
        }
        puzle1Resolution();
    }

    public void PuzleReset()
    {
        foreach (SO_Puzle1 item in puzle1)
        {
            if (item.name == "Trigger1")
                item.trigger = false;

            if (item.name == "Trigger2")
                item.trigger = false;

            if (item.name == "Trigger3")
                item.trigger = false;

            if (item.name == "Trigger4")
                item.trigger = false;
        }
    }

    IEnumerator PuzleDone()
    {
        yield return new WaitForSecondsRealtime(0.7f);
        doorPuzle1.SetActive(false);
        pStatus.puzleDoor = true;
    }
}
