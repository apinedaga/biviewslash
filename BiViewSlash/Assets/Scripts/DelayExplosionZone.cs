﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayExplosionZone : MonoBehaviour
{
    [SerializeField] private SO_Player player;
    [SerializeField] private float damage = 50;
    [SerializeField] private GameObject Explosion;
    private float checkCollisionsTime = 0.15f;
    [SerializeField] private HpManagerEvent hpEvent;

    private void Awake()
    {
        this.gameObject.GetComponent<MeshCollider>().enabled = false;
    }
    public void setMyExplosionTime(float time)
    {
        StartCoroutine(this.explosionTime(time));
    }

    private IEnumerator explosionTime(float t)
    {
        yield return new WaitForSecondsRealtime(t - this.checkCollisionsTime);
        this.gameObject.GetComponent<MeshCollider>().enabled = true;
        yield return new WaitForSecondsRealtime(this.checkCollisionsTime);
        this.gameObject.GetComponent<MeshCollider>().enabled = false;
        Instantiate(this.Explosion).transform.position = this.transform.position;
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player" && !player.invencible)
        {
            this.hpEvent.RaiseItem(this.damage);
        }
    }
}
