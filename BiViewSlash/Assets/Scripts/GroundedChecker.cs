﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedChecker : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        this.gameObject.GetComponentInParent<Moviment>().setGrounded(false);
    }

    private void OnTriggerStay(Collider other)
    {
        this.gameObject.GetComponentInParent<Moviment>().setGrounded(true);
    }
}
