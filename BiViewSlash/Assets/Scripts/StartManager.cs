﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
 * Classe al començar una partida seteja tots els scriptable objects a default value.
 */
public class StartManager : MonoBehaviour
{
    public GameObject manager;

    public void StartGame()
    {
        foreach (CameraType item in manager.GetComponent<ScriptableObjectManager>().camType)
        {
            item.cenital = true;
        }

        foreach (SO_DeadEnemies item in manager.GetComponent<ScriptableObjectManager>().deadEnemies)
        {
            item.deadEnemies.Clear();
            item.loaded = false;
        }

        foreach (SO_LastRespawn item in manager.GetComponent<ScriptableObjectManager>().lastRespawn)
        {
            item.lastRespawn = new Vector3(0, 0, 0);
        }

        foreach (SO_Player item in manager.GetComponent<ScriptableObjectManager>().player)
        {
            item.hpMax = 500;
            item.hp = item.hpMax;
            item.mpMax = 50;
            item.mp = 0;
            item.attack = 30;
            item.defense = 15;
            item.onCenitalMode = true;
            item.invencible = false;
            item.loaded = false;
            item.savedPosition = new Vector3(0, 0, 0);
        }

        foreach (SO_Puzle1 item in manager.GetComponent<ScriptableObjectManager>().puzle1)
        {
            item.trigger = false;
        }

        foreach (SO_PuzlesStatus item in manager.GetComponent<ScriptableObjectManager>().puzleStatus)
        {
            item.puzleDoor = false;
            item.puzleBossLeft = false;
            item.puzleBossRight = false;
        }

        foreach (SO_respawn item in manager.GetComponent<ScriptableObjectManager>().respawn)
        {
            item.respawnActive = false;
        }

        foreach (SO_RespawnsStatus item in manager.GetComponent<ScriptableObjectManager>().respawnStatus)
        {
            item.respawnBossDoor = false;
            item.respawnBossFloor = false;
            item.respawnFirstDoor = false;
        }

        foreach (SO_Sound item in manager.GetComponent<ScriptableObjectManager>().sound)
        {
            item.soundVolume = 0.5f;
        }

        foreach (MiniPuzzleStatus item in manager.GetComponent<ScriptableObjectManager>().miniPuzleStatus)
        {
            item.solved1 = false;
            item.solved2 = false;
        }

        foreach (TutorialPuzzleStatus item in manager.GetComponent<ScriptableObjectManager>().tutoPuzle)
        {
            item.solved1 = false;
            item.solved2 = false;
            item.solved3 = false;
        }
        SceneManager.LoadScene("MinitutorialAutomatic");
    }

    public void LoadGame()
    {
        GetComponent<SaveManager>().LoadGame();
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
