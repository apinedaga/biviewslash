﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathEnemy : MonoBehaviour
{
    public GameObject saveManager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void death()
    {
        saveManager.GetComponent<SaveManager>().deadEnemies.deadEnemies.Add(this.gameObject.name);
        //Destroy(this.gameObject);
        this.gameObject.SetActive(false);
    }
}
