﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHurtBox : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlayerHitBox"))
        {
            //this.GetComponentInParent<DeathEnemy>().death();
            this.GetComponentInParent<BossManager>().ReciveDamage(GetComponentInParent<BossManager>().playerStatus.attack);
        }

        if (other.CompareTag("PlayerHitBox2"))
        {
            //this.GetComponentInParent<DeathEnemy>().death();
            this.GetComponentInParent<BossManager>().ReciveDamage(GetComponentInParent<BossManager>().playerStatus.attack+10);
        }

        if (other.CompareTag("Fire"))
        {
            //this.GetComponentInParent<DeathEnemy>().death();
            this.GetComponentInParent<BossManager>().ReciveDamage(100);
        }
    }
}
