﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class detectarPJBoss : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            this.GetComponentInParent<BossManager>().setPlayerInRange(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            this.GetComponentInParent<BossManager>().setPlayerInRange(false);
        }
    }
}
