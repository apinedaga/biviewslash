﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossTrigger : MonoBehaviour
{
    private float bossY = 5f;
    public GameObject mainCam;
    public Camera cam;
    public GameObject Boss;
    private bool emergingBoss;
    public GameObject respawn;
    public GameObject StartEffectBoss;
    private int count;
    public GameObject[] spotsEffect;
    public SO_Player player;
    // Start is called before the first frame update
    void Start()
    {
        count = StartEffectBoss.transform.childCount;
        cam.gameObject.SetActive(false);
        Boss.transform.position = new Vector3(Boss.transform.position.x, 0, Boss.transform.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        /*
         * Al activar el bool el boss, comença a pujar.
         */
        if(emergingBoss && Boss.transform.position.y < bossY)
        {
            Boss.transform.position = new Vector3(Boss.transform.position.x, Boss.transform.position.y+0.05f, Boss.transform.position.z);
        }
    }

    /*
     * Quan el trigger detecta al pj comença el efecte per la aparició del boss
     * Activa els efectes de fum per l'aparició del boss.
     * Activa la corrutina per desactivar la camera que està fixe al boss.
     */
    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //Time.timeScale = 0;
            mainCam.SetActive(false);
            cam.gameObject.SetActive(true);
            emergingBoss = true;
            for (int i = 0; i < count; i++)
            {
                GameObject go = Instantiate(StartEffectBoss.transform.GetChild(i).gameObject);
                go.transform.position = spotsEffect[i].transform.position;
            }
            respawn.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
            respawn.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
            respawn.transform.GetChild(2).GetComponent<MeshRenderer>().enabled = false;
            player.cinematic = true;
            StartCoroutine(BossCam());
        }
    }

    /*
     * Corrutina per desactivar la camera que fixa al boss
     */
    IEnumerator BossCam()
    {
        yield return new WaitForSecondsRealtime(5f);
        cam.gameObject.SetActive(false);
        mainCam.SetActive(true);
        //Time.timeScale = 1;
        emergingBoss = false;
        //this.gameObject.SetActive(false);
        Boss.GetComponent<BossManager>().startBoss();
        player.cinematic = false;
        this.gameObject.SetActive(false);
    }
}
