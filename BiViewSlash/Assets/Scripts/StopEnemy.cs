﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopEnemy : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            this.gameObject.GetComponentInParent<followPj>().mustStop = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            this.gameObject.GetComponentInParent<followPj>().mustStop = false;
        }
    }
}
