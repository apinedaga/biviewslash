﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerFirstDoor : MonoBehaviour
{
    public SO_TriggersEnemies t;
    public SO_TriggersEnemies.triggerPos area;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            t.currentArea = area;
            print(area);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            t.currentArea = SO_TriggersEnemies.triggerPos.NONE;
        }
    }
}
