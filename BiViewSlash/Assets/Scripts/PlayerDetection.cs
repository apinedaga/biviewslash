﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetection : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            this.GetComponentInParent<EnemyNavmesh>().setDetected(true);
            //this.GetComponentInParent<followPj>().setIsInRange(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            this.GetComponentInParent<EnemyNavmesh>().setDetected(false);
            //this.GetComponentInParent<followPj>().setIsInRange(false);
        }
    }
}
