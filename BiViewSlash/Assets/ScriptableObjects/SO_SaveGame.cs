﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_SaveGame : ScriptableObject
{
    public Vector3 playerPosition;
    public float hp;
    public float mp;
    public string[] deadEnemies;
    public GameObject[] puzles;
    public string currentScene;
    public SO_respawn[] respawn;
    public SO_Puzle1[] puzle1;
    public bool puzleDoor;
    public bool puzleBossRight;
    public bool puzleBossLeft;
    public bool respawnFirstDoor;
    public bool respawnBossDoor;
    public bool respawnBossFloor;
    public Vector3 lastRespawnPosition;
    public float soundValue;
}
