﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpManagerEventListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public HpManagerEvent Event;

    public SO_Player stats;

    private void Start()
    {
        
    }

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(float it)
    {
        stats.hp = stats.hp - it;
        this.GetComponentInParent<HealthBarChange>().updateHealthBar();
    }
}
