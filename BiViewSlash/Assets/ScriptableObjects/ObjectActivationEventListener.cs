﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectActivationEventListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public ObjectActivationEvent Event;

    public CameraType camType;


    private void Start()
    {

    }

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(GameObject it)
    {
        if (camType.cenital)
            it.GetComponent<ObjectActivation>().SetActive();
        else
            it.GetComponent<ObjectActivation>().SetInactive();
    }
}
