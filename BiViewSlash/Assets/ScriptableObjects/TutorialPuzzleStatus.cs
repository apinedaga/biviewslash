﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TutorialPuzzleStatus : ScriptableObject
{
    public bool solved1;
    public bool solved2;
    public bool solved3;
}
