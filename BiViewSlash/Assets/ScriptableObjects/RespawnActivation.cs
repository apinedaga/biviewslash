﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnActivation : MonoBehaviour
{
    public RespawnActivationEvent objectEvent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetActive()
    {
        this.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
        this.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = true;
        this.transform.GetChild(2).GetComponent<MeshRenderer>().enabled = true;
    }

    public void SetInactive()
    {
        this.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
        this.transform.GetChild(1).GetComponent<MeshRenderer>().enabled = false;
        this.transform.GetChild(2).GetComponent<MeshRenderer>().enabled = false;
    }
}
