﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_EnemyStats : ScriptableObject
{
    public float maxHp;
    public float attack;
    public float defense;
}
