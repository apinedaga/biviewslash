﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_PuzlesStatus : ScriptableObject
{
    public bool puzleDoor;
    public bool puzleBossRight;
    public bool puzleBossLeft;
}
