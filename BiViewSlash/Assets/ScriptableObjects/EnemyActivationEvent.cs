﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemyActivationEvent : ScriptableObject
{
    // Start is called before the first frame update
    private readonly List<EnemyActivationEventListener> eventListeners = new List<EnemyActivationEventListener>();

    public void RaiseItem(GameObject it)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(it);
    }

    public void RegisterListener(EnemyActivationEventListener listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(EnemyActivationEventListener listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
