﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_RespawnsStatus : ScriptableObject
{
    public bool respawnFirstDoor;
    public bool respawnBossDoor;
    public bool respawnBossFloor;
}
