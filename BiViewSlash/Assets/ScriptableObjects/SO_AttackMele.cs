﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_AttackMele : ScriptableObject
{
    public float damage;
    public int[] comboSize;
    public int numberHitsCombo;
}
