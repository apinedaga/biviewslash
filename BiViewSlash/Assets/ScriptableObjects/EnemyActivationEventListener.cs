﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActivationEventListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public EnemyActivationEvent Event;

    public CameraType camType;


    private void Start()
    {

    }

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(GameObject it)
    {
        if (camType.cenital)
            it.GetComponent<EnemyActivation>().SetActive();
        else
            it.GetComponent<EnemyActivation>().SetInactive();
    }
}
