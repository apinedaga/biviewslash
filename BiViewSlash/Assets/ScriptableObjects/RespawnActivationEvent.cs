﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class RespawnActivationEvent : ScriptableObject
{
    private readonly List<RespawnActivationEventListener> eventListeners = new List<RespawnActivationEventListener>();

    public void RaiseItem(GameObject it)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(it);
    }

    public void RegisterListener(RespawnActivationEventListener listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(RespawnActivationEventListener listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
