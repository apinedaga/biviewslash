﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ObjectManagerEvent : ScriptableObject
{
    private readonly List<ObjectPositionEventListener> eventListeners = new List<ObjectPositionEventListener>();

    public void RaiseItem(GameObject it)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(it);
    }

    public void RegisterListener(ObjectPositionEventListener listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(ObjectPositionEventListener listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
