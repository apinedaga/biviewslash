﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_TriggersEnemies : ScriptableObject
{
    public enum triggerPos { NONE, FIRSTDOOR, SECONDDOOR, RIGHTACTIVATORTRUE, LEFTACTIVATORTRUE, LEFTACTIVATORFALSE, LEFFIRSTDOORPLATFORM , RIGHTSECOND, RIGHTSECONDUP, LEFTSECONDONE, LEFTSECONDTWO, LEFTSECONDUP};
    public triggerPos currentArea;
}
