﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BossPuzleStatus : ScriptableObject
{
    public bool rightSprereActive = false;
    public bool leftSphereActive = false;
}
