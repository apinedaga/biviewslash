﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableObjectManager : MonoBehaviour
{
    public SO_AttackMele[] atk;
    public SO_EnemyStats[] enemies;
    public SO_DeadEnemies[] deadEnemies;
    public SO_LastRespawn[] lastRespawn;
    public SO_Player[] player;
    public SO_Puzle1[] puzle1;
    public SO_PuzlesStatus[] puzleStatus;
    public SO_respawn[] respawn;
    public SO_RespawnsStatus[] respawnStatus;
    public SO_SaveGame[] saveGame;
    public CameraType[] camType;
    public PlayerPublicStats[] publicStats;
    public SO_Sound[] sound;
    public MiniPuzzleStatus[] miniPuzleStatus;
    public TutorialPuzzleStatus[] tutoPuzle;
}
