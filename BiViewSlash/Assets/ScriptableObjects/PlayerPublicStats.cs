﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerPublicStats : ScriptableObject
{
    public Vector3 posicio;
    public bool onCenitalMode;
}
