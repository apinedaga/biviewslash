﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_respawn : ScriptableObject
{
    public string nameRespawn;
    public bool respawnActive;
}
