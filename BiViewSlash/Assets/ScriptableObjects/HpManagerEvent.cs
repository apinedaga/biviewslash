﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class HpManagerEvent : ScriptableObject
{

    private readonly List<HpManagerEventListener> eventListeners = new List<HpManagerEventListener>();

    public void RaiseItem(float it)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(it);
    }

    public void RegisterListener(HpManagerEventListener listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(HpManagerEventListener listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }

}
