﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_LastRespawn : ScriptableObject
{
    public Vector3 lastRespawn;
}
