﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class MiniPuzzleStatus : ScriptableObject
{
    public bool solved1;
    public bool solved2;
}
