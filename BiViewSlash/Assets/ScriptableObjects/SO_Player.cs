﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_Player : ScriptableObject
{
    public float hpMax;
    public float hp;
    public float mpMax;
    public float mp;
    public float attack;
    public float defense;
    public Vector3 posicio;
    public bool onCenitalMode;
    public bool invencible;
    public bool loaded;
    public bool loadPos;
    public Vector3 savedPosition;
    public bool cinematic;
}
