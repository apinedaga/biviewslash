﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_DeadEnemies : ScriptableObject
{
    public List<string> deadEnemies = new List<string>();
    public bool loaded;

}
