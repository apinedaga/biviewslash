﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SO_Sound : ScriptableObject
{
    public float soundVolume;
    public bool loaded;
}
