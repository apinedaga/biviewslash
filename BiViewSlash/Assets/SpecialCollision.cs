﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialCollision : MonoBehaviour
{
    public enum camChangeType { FAR, NEAR}
    public camChangeType cmType;
    public Camera camLat;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            print("HOLA");
            if (cmType == camChangeType.FAR)
                camLat.GetComponent<Camera>().farClipPlane = 130f;
            else if (cmType == camChangeType.NEAR)
                camLat.GetComponent<Camera>().farClipPlane = 30f;

        }
    }
}
